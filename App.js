import AppLoading from 'expo-app-loading';
import {Asset} from 'expo-asset';
import * as Font from 'expo-font';
import React, {useState, useEffect} from 'react';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import {Provider, useDispatch, useSelector} from 'react-redux';
import ErrorBoundary from "./src/components/error-boundry/ErrorBoundary";
import ApiService from "./src/services/api";
import {ApiServiceProvider} from "./src/components/api-service-context"
import {store, persistor} from './src/store'
import {PersistGate} from 'redux-persist/integration/react'
import FlashMessage from "react-native-flash-message";
import {I18nextProvider} from 'react-i18next';
import i18n from './src/services/i18n';

import AppNavigator from './src/navigation/AppNavigator';
import {itemsFetch, profileFetch} from "./src/actions";

import Spinner from "./src/components/spinner/Spinner";

const apiService = new ApiService();

if (__DEV__) {
    import('./src/utils/reactotron/reactotron').then(() => console.log('Reactotron Configured'))
}

export default function App(props) {
    const [isLoadingComplete, setLoadingComplete] = useState(false);

    if (!isLoadingComplete && !props.skipLoadingScreen) {

        return (
            <AppLoading
                startAsync={loadResourcesAsync}
                onError={handleLoadingError}
                onFinish={() => handleFinishLoading(setLoadingComplete)}
            />
        );
    } else {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <I18nextProvider i18n={i18n()}>
                        <ErrorBoundary>
                            <ApiServiceProvider value={apiService}>
                                <View style={styles.container}>
                                    {Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
                                    <Main/>
                                    <FlashMessage position="bottom"/>
                                </View>
                            </ApiServiceProvider>
                        </ErrorBoundary>
                    </I18nextProvider>
                </PersistGate>
            </Provider>
        );
    }
}

const Main = () => {
    const token = useSelector(state => state.auth.token);
    const user = useSelector(state => state.user);
    const dispatch = useDispatch();

    useEffect(() => {
        if (token) itemsFetch(apiService, dispatch, 'notification', 'notifications', token);
    });

    useEffect(() => {
        if (token) profileFetch(apiService, dispatch, token);
    }, [token]);

    return <AppNavigator i18n={ i18n }/>
};

async function loadResourcesAsync() {
    await Promise.all([
        Asset.loadAsync([
            require('./src/assets/images/robot-dev.png'),
            require('./src/assets/images/robot-prod.png'),
        ]),
        Font.loadAsync({
            // This is the font that we are using for our tab bar
            ...Ionicons.font,
            // We include SpaceMono because we use it in HomeScreen.js. Feel free to
            // remove this if you are not using it in your app
            'space-mono': require('./src/assets/fonts/SpaceMono-Regular.ttf'),
            'Lato': require('./src/assets/fonts/Lato-Regular.ttf'),
            'LatoMedium': require('./src/assets/fonts/Lato-Medium.ttf'),
            'LatoBold': require('./src/assets/fonts/Lato-Bold.ttf'),
            'LatoBlack': require('./src/assets/fonts/Lato-Black.ttf'),
        }),
    ]);
}

function handleLoadingError(error) {
    // In this case, you might want to report the error to your error reporting
    // service, for example Sentry
    console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
    setLoadingComplete(true);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});


//v
