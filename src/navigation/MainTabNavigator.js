import React from 'react';
import { Platform, Dimensions } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import Colors from "../constants/Colors";
import Icon from "../constants/Icons";
import FeedScreen from "../screens/user/FeedScreen";
import HomeCompaniesScreen from "../screens/HomeCompaniesScreen";
import ClientsScreen from "../screens/user/ClientsScreen";
import ProvidersScreen from "../screens/user/ProvidersScreen";
import CategoriesScreen from "../screens/CategoriesScreen";
import SettingsScreen from "../screens/user/SettingsScreen";
import InterestsScreen from "../screens/user/InterestsScreen";
import TabBar from "../components/TabBar";
import ProductsScreen from "../screens/ProductsScreen";
import ProductDetailsScreen from "../screens/ProductDetailsScreen";
import CompaniesScreen from "../screens/CompaniesScreen";
import MyShopScreen from "../screens/user/MyShopScreen";
import i18next from 'i18next';

import LaunchScreen from "../screens/LaunchScreen";
import {withTranslation} from "react-i18next";
import {get} from "lodash";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
})
const lang = get(i18next,'languages[0]','')
const t = get(i18next,`options.resources.${lang}.main`,'')
const HomeStack = createStackNavigator(
    {

        Home: {
            screen:HomeScreen,
            navigationOptions:{
                header: null
            }
        },
        Categories: {
            screen: CategoriesScreen,
            navigationOptions: {
                header: null
            }
        },
        products: {
            screen: ProductsScreen,
            navigationOptions: {
                header: null
            }
        },
        ProductDetails: {
            screen: ProductDetailsScreen,
            navigationOptions: {
                header: null
            }
        },
    },
    config
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Рынок',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="market" width={24} height={24} fill={tintColor}/>
    ),
};

HomeStack.path = '';

const CompaniesStack = createStackNavigator(
    {
        Companies: HomeCompaniesScreen,
        companies: {
            screen: CompaniesScreen,
            navigationOptions: { header: null }
        },
        Categories: {
            screen: CategoriesScreen,
            navigationOptions: {
                header: null
            }
        },
    },
    config
);

CompaniesStack.navigationOptions = {
    tabBarLabel: 'Предприятия',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="companies" width={24} height={24} fill={tintColor}/>
    ),
};

CompaniesStack.path = '';

const ClientsStack = createStackNavigator(
    {
        Clients: ClientsScreen,
    },
    config
);

ClientsStack.navigationOptions = {
    // const i18next = get(i18next)
    // console.log("TABBBB:",i18next)
    tabBarLabel:"Blabla",
    // t.search,
        // 'Клиенты',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="user" width={24} height={24} fill={tintColor}/>
    ),
};

ClientsStack.path = '';

const ProvidersStack = createStackNavigator(
    {
        Providers: ProvidersScreen,
    },
    config
);

ProvidersStack.navigationOptions = {
    tabBarLabel: 'Поставщики',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="providers" width={24} height={24} fill={tintColor}/>
    ),
};

ProvidersStack.path = '';

const FeedStack = createStackNavigator(
    {
        Feed: {
            screen: FeedScreen,
            navigationOptions: {
                header: null
            }
        }
    },
    config
);

FeedStack.navigationOptions = {
    tabBarLabel: 'Лента',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="feed" width={24} height={24} fill={tintColor}/>
    ),
};

FeedStack.path = '';

const SettingStack = createStackNavigator(
    {
        Settings: {
            screen:SettingsScreen,
            navigationOptions: {
                header: null
            }
        }
    },
    config
);

SettingStack.navigationOptions = {
    header: null,
    tabBarLabel: 'Настройки',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="settings" width={24} height={24} fill={tintColor}/>
    ),
};

SettingStack.path = '';

const InterestStack = createStackNavigator(
    {
        Interests: {
            screen: InterestsScreen,
            navigationOptions: {
                header: null
            }}
    },
    config
);

InterestStack.navigationOptions = {
    tabBarLabel: 'Интересы',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="star" width={24} height={24} fill={tintColor}/>
    ),
};

InterestStack.path = '';

const ShopStack = createStackNavigator(
    {
        MyShop:{
            screen:MyShopScreen,
            navigationOptions: {
                header: null
            }
        }

    },
    config
);

ShopStack.navigationOptions = {
    tabBarLabel: 'Мой Магазин',
    tabBarIcon: ({ tintColor }) => (
        <Icon name="star" width={24} height={24} fill={tintColor}/>
    ),
};

ShopStack.path = '';

const tabNavigator = createBottomTabNavigator({
    FeedStack,
    HomeStack,
    CompaniesStack,
    ClientsStack,
    ProvidersStack,
    ShopStack,
    SettingStack,
    InterestStack,
},{
    initialRouteName: 'HomeStack',
    tabBarComponent: props => <TabBar {...props}/>,
    swipeEnabled: true,
    tabBarOptions: {
        scrollEnabled: true,
        style: {
            paddingTop: 8,
            height: 50,
            backgroundColor: Colors.tintColor,
            borderTopWidth: 0,
        },
        tabStyle:{
            width: Dimensions.get('window').width / 3,
        },
        labelStyle: {
            fontFamily: 'LatoBold',
            fontSize: 12
        },
        activeTintColor: Colors.gold,
        inactiveTintColor: Colors.white
    }
});

tabNavigator.path = '';

export default (tabNavigator);
