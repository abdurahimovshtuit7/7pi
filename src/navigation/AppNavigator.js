import React from 'react';
import {createAppContainer, createStackNavigator, createSwitchNavigator} from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import SearchBar from "../components/SearchBar";
import AuthButton from "../components/button/AuthButton";
import {LinearGradient} from "expo-linear-gradient";
import MenuButton from "../components/button/MenuButton";
import CompanyDetailsScreen from "../screens/CompanyDetailsScreen";
import R from "../constants/R";
import ShopScreen from "../screens/ShopScreen";
import NotificationScreen from "../screens/user/NotificationScreen";
import LoginScreen from "../screens/auth/LoginScreen";
import SignupScreen from "../screens/auth/SignupScreen";
import SearchScreen from "../screens/SearchScreen";
import EditPostScreen from "../screens/user/EditPostScreen";
import SelectCategory from "../screens/user/SelectCategory";
import AdFormScreen from "../screens/user/AdFormScreen";
import ProductDetailsScreen from "../screens/ProductDetailsScreen";
import LaunchScreen from "../screens/LaunchScreen";

export default createAppContainer(
    createStackNavigator({
        Launch: {
            screen:LaunchScreen,
            navigationOptions:{
                header: null
            }
        },
        Root: {
            screen: createSwitchNavigator({
                // You could add another route here for authentication.
                // Read more at https://reactnavigation.org/docs/en/auth-flow.html
                Main: MainTabNavigator,
            }),
            navigationOptions: {
                // header: null,
                headerTitle: <SearchBar/>,
                headerBackTitle: null,
                headerTintColor: '#ffffff',
                headerBackground:
                    (
                    <LinearGradient
                        colors={['#003D79', '#001831']}
                        style={{ flex: 1 ,paddingBottom: 20}}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                    />
                ),
                headerStyle: {
                    // paddingTop:Platform.OS==='ios'?55:0,
                    height:Platform.OS==='ios'?70:30,
                    paddingBottom:Platform.OS==='ios'?0:20,
                    // borderWidth:1,
                    // justifyContent:'center',
                    // alignItems:'center',
                    // backgroundColor: '#f4511e',
                },
                headerLeft:
                    <MenuButton/>,
                headerRight:
                    <AuthButton/>
            }
        },

        CompanyDetails: {
            screen: CompanyDetailsScreen,
            navigationOptions: R.palettes.header
        },
        Shop: {
            screen: ShopScreen,
            navigationOptions: R.palettes.header
        },
        ProductDetails: {
            screen: ProductDetailsScreen,
            navigationOptions: R.palettes.header
        },
        Search: {
            screen: SearchScreen,
            navigationOptions: R.palettes.header
        },
        EditPost: {
            screen: EditPostScreen,
            navigationOptions: R.palettes.header
        },
        SelectCategory: {
            screen: SelectCategory,
            navigationOptions: R.palettes.header
        },
        AdForm: {
            screen: AdFormScreen,
            navigationOptions: R.palettes.header
        },
        Notification: {
            screen: NotificationScreen,
            navigationOptions: R.palettes.header
        },
        Login: {
            screen: LoginScreen,
            navigationOptions: R.palettes.header
        },
        Signup: {
            screen: SignupScreen,
            navigationOptions: R.palettes.header
        },
    })
);
