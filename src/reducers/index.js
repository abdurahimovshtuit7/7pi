import {
    categories,
    subcategories,
    company,
    companies,
    shop,
    product,
    products,
    user,
    providers,
    clients,
    feed,
    notifications,
    profile,
    interests,
    searchProducts,
    searchCompanies,
    search,
    SET_LANGUAGE,
    PAGE
} from "../constants/actionTypes";

const initialState = {
    items:{
        items: [],
        loading: false,
        error: null,
    },
    categories:{
        items: [],
        loading: false,
        error: null,
    },
    subcategories:{
        items: [],
        loading: false,
        error: null,
    },
    products:{
        items: [],
        loading: false,
        error: null,
    },
    product: {
        item: null,
        loading: true,
        error: null,
    },
    company: {
        item: null,
        loading: true,
        error: null,
    },
    shop:{
        items: [],
        loading: true,
        error: null,
    },
    companies:{
        items: [],
        loading: false,
        error: null,
    },
    clients:{
        items: [],
        loading: true,
        error: null,
    },
    providers:{
        items: [],
        loading: true,
        error: null,
    },
    feed:{
        items: [],
        loading: true,
        error: null,
        time: new Date()
    },
    notifications:{
        items: [],
        loading: true,
        error: null,
    },
    searchProducts:{
        items: [],
        loading: true,
        error: null,
    },
    searchCompanies:{
        items: [],
        loading: true,
        error: null,
    },
    interests:{
        items: [],
        loading: true,
        error: null,
    },
    auth: {
        token: null,
        user: 0,
        loading: false,
        error: null,
    },
    user: null,
    selectedProduct: null,
    selectedCompany: null,
    search: null,
    updateComments: null,
    lang:'uz',
    page:1,
};

const reducer = ( state = initialState, action) => {

    console.log(action.type)

    switch (action.type) {

        case search.UPDATE:
            return {
                ...state,
                search: action.payload
            };
        case PAGE:
            return {
                ...state,
                page: action.payload
            };

        case product.SELECT:
            return {
                ...state,
                selectedProduct: action.payload
            };

        case companies.SELECT:
            return {
                ...state,
                selectedCompany: action.payload
            };

        case categories.REQUEST:
            return {
                ...state,
                categories:{
                    items: [],
                    loading: true,
                    error: null,
                }
            };
        case categories.SUCCESS:
            return {
                ...state,
                categories:{
                    items: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case categories.FAILURE:
            return {
                ...state,
                categories:{
                    items: [],
                    loading: false,
                    error: action.payload,
                }
            };
        case subcategories.REQUEST:
            return {
                ...state,
                subcategories:{
                    items: [],
                    loading: true,
                    error: null,
                }
            };
        case subcategories.SUCCESS:
            return {
                ...state,
                subcategories:{
                    items: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case subcategories.FAILURE:
            return {
                ...state,
                subcategories:{
                    items: [],
                    loading: false,
                    error: action.payload,
                }
            };
        case products.REQUEST:
            return {
                ...state,
                products:{
                    items: [],
                    loading: true,
                    error: null,
                }
            };
        case products.SUCCESS:
            return {
                ...state,
                products:{
                    items: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case products.FAILURE:
            return {
                ...state,
                products:{
                    items: [],
                    loading: false,
                    error: action.payload,
                }
            };
        case user.LOGIN_REQUEST:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: true,
                    error: null,
                }
            };
        case user.LOGIN_SUCCESS:
            return {
                ...state,
                auth:{
                    token: action.payload,
                    loading: false,
                    error: null,
                }
            };

        case user.LOGOUT:
            return {
                ...state,
                shop: initialState.shop,
                auth: initialState.auth,
                feed: initialState.feed,
                clients: initialState.clients,
                providers: initialState.providers,
                notifications: initialState.notifications,
                interests: initialState.interests,
                user: initialState.user
            };
        case user.LOGIN_FAILURE:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: false,
                    error: action.payload,
                }
            };
        case user.REGISTER_REQUEST:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: true,
                    error: null,
                }
            };
        case user.REGISTER_SUCCESS:
            return {
                ...state,
                auth:{
                    token: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case user.REGISTER_FAILURE:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: false,
                    error: action.payload,
                }
            };
        case product.REQUEST:
            return {
                ...state,
                product: {
                    item: null,
                    loading: true,
                    error: null,
                },
            };
        case product.SUCCESS:
            return {
                ...state,
                product: {
                    item: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case product.FAILURE:
            return {
                ...state,
                product: {
                    item: null,
                    loading: false,
                    error: action.payload,
                },
            };
        case company.REQUEST:
            return {
                ...state,
                company: {
                    item: null,
                    loading: true,
                    error: null,
                },
            };
        case company.SUCCESS:
            return {
                ...state,
                company: {
                    item: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case company.FAILURE:
            return {
                ...state,
                company: {
                    item: null,
                    loading: false,
                    error: action.payload,
                },
            };
        case profile.REQUEST:
            return {
                ...state,
                user: {
                    id: 0,
                    clients: [],
                    providers: [],
                    shareds: [],
                    plan: false,
                    error: null,
                    loading: true
                },
            };
        case profile.SUCCESS:
            return {
                ...state,
                user: {
                    id: action.payload.user,
                    clients: action.payload.clients,
                    providers: action.payload.providers,
                    shareds: action.payload.shareds,
                    plan: action.payload.plan,
                    error: null,
                    loading: false
                },
            };
        case profile.FAILURE:
            return {
                ...state,
                user: initialState.user,
            };
        case profile.ADD_PROVIDER:
            return {
                ...state,
                user: {
                    ...state.user,
                    providers: [
                        ...state.user.providers,
                        action.payload
                    ]
                },
            };
        case profile.ADD_CLIENT:
            return {
                ...state,
                user: {
                    ...state.user,
                    clients: [
                        ...state.user.clients,
                        action.payload
                    ]
                },
            };
        case companies.REQUEST:
            return {
                ...state,
                companies: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case companies.SUCCESS:
            return {
                ...state,
                companies: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case companies.FAILURE:
            return {
                ...state,
                companies: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case shop.REQUEST:
            return {
                ...state,
                shop: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case shop.SUCCESS:
            return {
                ...state,
                shop: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case shop.FAILURE:
            return {
                ...state,
                shop: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case clients.REQUEST:
            return {
                ...state,
                clients: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case clients.SUCCESS:
            return {
                ...state,
                clients: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case clients.FAILURE:
            return {
                ...state,
                clients: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case providers.REQUEST:
            return {
                ...state,
                providers: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case providers.SUCCESS:
            return {
                ...state,
                providers: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case providers.FAILURE:
            return {
                ...state,
                providers: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case feed.REQUEST:
            return {
                ...state,
                feed: {
                    ...state.feed,
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case feed.SUCCESS:
            return {
                ...state,
                feed: {
                    ...state.feed,
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case feed.UPDATE:
            return {
                ...state,
                feed: {
                    ...state.feed,
                    time: action.payload,
                },
            };
        case feed.UPDATE_COMMENTS:
            return {
                ...state,
                updateComments: action.payload,
            };
        case feed.FAILURE:
            return {
                ...state,
                feed: {
                    ...state.feed,
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case notifications.REQUEST:
            return {
                ...state,
                notifications: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case notifications.SUCCESS:
            return {
                ...state,
                notifications: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case notifications.FAILURE:
            return {
                ...state,
                notifications: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case searchProducts.REQUEST:
            return {
                ...state,
                searchProducts: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case searchProducts.SUCCESS:
            return {
                ...state,
                searchProducts: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case searchProducts.FAILURE:
            return {
                ...state,
                searchProducts: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case searchCompanies.REQUEST:
            return {
                ...state,
                searchCompanies: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case searchCompanies.SUCCESS:
            return {
                ...state,
                searchCompanies: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case searchCompanies.FAILURE:
            return {
                ...state,
                searchCompanies: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case interests.REQUEST:
            return {
                ...state,
                interests: {
                    items: [],
                    loading: true,
                    error: null,
                },
            };
        case interests.SUCCESS:
            return {
                ...state,
                interests: {
                    items: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case interests.FAILURE:
            return {
                ...state,
                interests: {
                    items: [],
                    loading: false,
                    error: action.payload,
                },
            };
        case SET_LANGUAGE:{
            return {
                ...state,
                lang: action.lang
            };
        }
        case interests.ADD:
            return {
                ...state,
                interests: {
                    items: {
                        ...state.interests.items,
                        selectedIds:[
                            ...state.interests.items.selectedIds,
                            action.payload
                        ]
                    },
                    loading: false,
                    error: null
                },
            };

        default: return state;
        }
};

export default reducer;
