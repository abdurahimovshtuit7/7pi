import strings from './Strings'
import images from './Images'
import colors from './Colors'
import fonts from './Fonts'
import palettes from './Palettes'

const R = {
    strings,
    images,
    colors,
    fonts,
    palettes
};

export default R;
