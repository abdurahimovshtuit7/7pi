const tintColor = '#002B55';
const white = '#FFFFFF';
const blue = '#00305f';
const ocean = '#005cb6';
const gold = '#ffc93e';
export default {
  tintColor,
  white,
  blue,
  gold,
  ocean,
  tabIconDefault: '#ccc',
  green: '#009a0f',
  bg: '#e9ebee',
  text: '#323232',
  muted: '#6e6e6e',
  border: '#F3F3F3',
  tabIconSelected: white,
  tabBar: '#fefefe',
  errorBackground: 'red',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
};
