export const categories = {
    REQUEST: 'FETCH_CATEGORIES_REQUEST',
    SUCCESS: 'FETCH_CATEGORIES_SUCCESS',
    FAILURE: 'FETCH_CATEGORIES_FAILURE',
};

export const subcategories = {
    REQUEST: 'FETCH_SUBCATEGORIES_REQUEST',
    SUCCESS: 'FETCH_SUBCATEGORIES_SUCCESS',
    FAILURE: 'FETCH_SUBCATEGORIES_FAILURE',
};

export const companies = {
    REQUEST: 'FETCH_COMPANIES_REQUEST',
    SUCCESS: 'FETCH_COMPANIES_SUCCESS',
    FAILURE: 'FETCH_COMPANIES_FAILURE',
    SELECT: 'SELECT_COMPANY'
};

export const shop = {
    REQUEST: 'FETCH_SHOP_REQUEST',
    SUCCESS: 'FETCH_SHOP_SUCCESS',
    FAILURE: 'FETCH_SHOP_FAILURE',
};

export const clients = {
    REQUEST: 'FETCH_CLIENTS_REQUEST',
    SUCCESS: 'FETCH_CLIENTS_SUCCESS',
    FAILURE: 'FETCH_CLIENTS_FAILURE',
};

export const providers = {
    REQUEST: 'FETCH_PROVIDERS_REQUEST',
    SUCCESS: 'FETCH_PROVIDERS_SUCCESS',
    FAILURE: 'FETCH_PROVIDERS_FAILURE',
};
export const SET_LANGUAGE = `SET_LANGUAGE`;
export const PAGE = `PAGE`;


export const feed = {
    UPDATE_COMMENTS: 'UPDATE_COMMENTS',
    REQUEST: 'FETCH_FEED_REQUEST',
    SUCCESS: 'FETCH_FEED_SUCCESS',
    UPDATE: 'FETCH_FEED_UPDATE',
    FAILURE: 'FETCH_FEED_FAILURE',
};

export const notifications = {
    REQUEST: 'FETCH_NOTIFICATIONS_REQUEST',
    SUCCESS: 'FETCH_NOTIFICATIONS_SUCCESS',
    FAILURE: 'FETCH_NOTIFICATIONS_FAILURE',
};

export const products = {
    REQUEST: 'FETCH_PRODUCTS_REQUEST',
    SUCCESS: 'FETCH_PRODUCTS_SUCCESS',
    FAILURE: 'FETCH_PRODUCTS_FAILURE',
};

export const search = {
    UPDATE: 'SEARCH_UPDATE'
};

export const searchProducts = {
    REQUEST: 'FETCH_SEARCH_PRODUCTS_REQUEST',
    SUCCESS: 'FETCH_SEARCH_PRODUCTS_SUCCESS',
    FAILURE: 'FETCH_SEARCH_PRODUCTS_FAILURE',
};

export const searchCompanies = {
    REQUEST: 'FETCH_SEARCH_COMPANIES_REQUEST',
    SUCCESS: 'FETCH_SEARCH_COMPANIES_SUCCESS',
    FAILURE: 'FETCH_SEARCH_COMPANIES_FAILURE',
};

export const interests = {
    REQUEST: 'FETCH_INTERESTS_REQUEST',
    SUCCESS: 'FETCH_INTERESTS_SUCCESS',
    FAILURE: 'FETCH_INTERESTS_FAILURE',
    ADD: 'INTERESTS_ADD',
    REMOVE: 'INTERESTS_REMOVE',
};

export const product = {
    REQUEST: 'FETCH_PRODUCT_REQUEST',
    SUCCESS: 'FETCH_PRODUCT_SUCCESS',
    FAILURE: 'FETCH_PRODUCT_FAILURE',
    SELECT: 'SELECT_PRODUCT'
};

export const company = {
    REQUEST: 'FETCH_COMPANY_REQUEST',
    SUCCESS: 'FETCH_COMPANY_SUCCESS',
    FAILURE: 'FETCH_COMPANY_FAILURE',
};

export const profile = {
    ADD_PROVIDER: 'ADD_PROVIDER',
    ADD_CLIENT: 'ADD_CLIENT',
    REQUEST: 'FETCH_PROFILE_REQUEST',
    SUCCESS: 'FETCH_PROFILE_SUCCESS',
    FAILURE: 'FETCH_PROFILE_FAILURE',
};

export const user = {
    REGISTER_REQUEST: 'REGISTER_REQUEST',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',

    LOGIN_REQUEST: 'LOGIN_REQUEST',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILURE: 'LOGIN_FAILURE',

    LOGOUT: 'LOGOUT',
};
