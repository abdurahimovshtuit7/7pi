import colors from './Colors'

import SearchBar from "../components/SearchBar";
import AuthButton from "../components/button/AuthButton";
import { LinearGradient } from 'expo-linear-gradient';
import React from "react";
import Icon from "./Icons";
import Colors from "./Colors";


import {TouchableOpacity} from "react-native";
// import navigation from '../navigation/AppNavigator'

export default {
    container: {
        flex: 1,
        backgroundColor: '#e8eaed',
        alignItems: 'center',
    },
    authContainer: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 20,
        paddingVertical: 32,
    },
    whiteContainer: {
        backgroundColor: '#ffffff',
        padding: 20,
    },
    whiteContainer0: {
        backgroundColor: '#ffffff',
    },
    flexBox:{
        flexDirection: 'row',
        marginBottom: 20,
    },
    flexRow:{

    },
    header: {
        headerTitle: <SearchBar/>,
        headerBackTitle: null,
        headerTintColor: '#ffffff',
        headerBackground: (
            <LinearGradient
                colors={['#003D79', '#001831']}
                style={{
                    flex: 1 ,
                    justifyContent:'center',
                    alignItems:'center',
                    height:Platform.OS==='ios'?70:40,
                    paddingBottom:Platform.OS==='ios'?0:20,
                }}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
            />
        ),
        headerRight:
            <AuthButton/>,
        headerStyle: {
            flex: 1,
            // paddingTop:Platform.OS==='ios'?55:0,
            height:Platform.OS==='ios'?70:40,
            paddingBottom:Platform.OS==='ios'?0:11,
            borderWidth:1,
            justifyContent:'center'
            // backgroundColor: '#f4511e',
        },
    },

}
