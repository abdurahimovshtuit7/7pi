import React  from "react";
import { Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window');

const strings = {
    name: '7Pi.uz',
    width: width,
    width2: Math.round(width/2),
    height: height,
};

export default strings
