import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

const PUSH_ENDPOINT = 'https://7pi.uz/rest/info';

export default async function registerForPushNotificationsAsync(token) {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }

    // Get the token that uniquely identifies this device
    let deviceToken = await Notifications.getExpoPushTokenAsync();

    let bodyFormData = new FormData();
    bodyFormData.append('deviceToken',deviceToken);

    return fetch(PUSH_ENDPOINT, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: bodyFormData,
    });
}
