
import {Platform} from "react-native";
import mime from "mime";

import Toast from "../components/Toast";
class ApiService {
    _apiBase = 'https://7pi.uz/rest/';

    async getResources(url,token=null){
        let headers = [];
        if(token){
            headers = {'Authorization': `Bearer ${token}`}
        }
        const res = await fetch(this._apiBase+url,{headers});

        if(!res.ok){
            throw new Error(`Could not fetch ${url}, received ${JSON.stringify(res)}`)

        }
        return await res.json();
        // console.log("RES:",res)

    }

    register = async(form) => {

        let bodyFormData = new FormData();
        bodyFormData.append('SignupForm[name]', `${form.name}`);
        bodyFormData.append('SignupForm[surname]', `${form.surname}`);
        bodyFormData.append('SignupForm[email]', `${form.email}`);
        bodyFormData.append('SignupForm[password]', `${form.password}`);
        bodyFormData.append('SignupForm[password_repeat]', `${form.password_repeat}`);
        bodyFormData.append('SignupForm[phone]', `${form.phone}`);

        return fetch(`https://7pi.uz/site/signup-mobile`,{
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: bodyFormData,
        }).then(res => {
            console.log("RES:",res)
            if(res.status!==200) throw new Error("Введены неправильные данные. Повторите заново!");
            return res.text();
        })
            .then((res)=>{
                const str = res.replace(/^"(.*)"$/, '$1');
                return str;
            })
    }

    login = async (form) => {
        let url = "https://7pi.uz/site/token";
        let bodyFormData = new FormData();
        bodyFormData.append('username',form.username);
        bodyFormData.append('password',form.password);

        return fetch(url, {
            method: 'POST',
            body: bodyFormData,
        })
            .then(res => {
                if(res.status!==200) throw new Error("Введены неправильные данные. Повторите заново!");
                return res.json();
            })
            .then((res)=>{
                return res;
            })

    };

    addPost = async (text,token,image=null,id=null) => {

        const url = id?'edit-post?id='+id:'add-post';

        let bodyFormData = new FormData();
        bodyFormData.append('Post[text]', text);


        if(image){
            bodyFormData.append('Post[image]', {
                uri: Platform.OS==='ios'?`data:image/jpeg;base64,`+image.base64:image.uri,
                name: 'selfie.jpg',
                type: 'image/jpg'
            });
        }

        return fetch(this._apiBase+url,{
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            },
            body: bodyFormData,
        }).then(res=>console.log(JSON.stringify(res))).catch(err=>console.log(err))
    };

    addComment = async (id, message, answer, token ) => {

        let bodyFormData = new FormData();
        bodyFormData.append("ad", id);
        bodyFormData.append("message", message);
        bodyFormData.append("answer", answer);

        return fetch(this._apiBase+'add-comment',{
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            },
            body: bodyFormData,
        }).then(res=>console.log(JSON.stringify(res))).catch(err=>console.log(err))
    };

    createAd = ( form, token, id=null ) => {

        const url = id?'add-offer?id='+id:'add-offer?id='+form.cat;

        let bodyFormData = new FormData();

        console.log("URL:",url)
        bodyFormData.append("Ad[title]", form.title);
        bodyFormData.append("Ad[text]", form.text);
        bodyFormData.append("Ad[price]", form.price);
        bodyFormData.append("Ad[pricen]", form.pricen);
        bodyFormData.append("Ad[pricer]", form.pricer);
        bodyFormData.append("Ad[city]", form.city);
        bodyFormData.append("Ad[address]", form.address);
        bodyFormData.append("Ad[phone]", form.phone);
        bodyFormData.append("Ad[email]", form.email);
        bodyFormData.append("Ad[shop]", form.shop);

        const photo = form.photo?form.photo.split('/'):null;
        if (form.photo) {
            bodyFormData.append(`Ad[photos][]`, {
                name: form.photo.name ? form.photo.name : photo[photo.length - 1],
                filename: form.photo.filename ? form.photo.filename : photo[photo.length - 1],
                type: mime.getType(form.photo),
                uri: form.photo
            });
            // bodyFormData.append("Ad[photos][]", form.photo);
        }
         console.log("Body:",bodyFormData)
        return fetch(this._apiBase+url,{
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            },
            body: bodyFormData,
        }).then(res =>  {return Toast.show("Success")}).catch(err=>console.log(err))
    };

    removePartner = ( id, token ) => {
        return this.getResources("removepartner?id="+id, token)
    };
    removeAd = (id, token) => {
        return this.getResources(`remove-ad?id=${id}`, token)
    }
    removeNotification = ( id, type, token ) => {
        let bodyFormData = new FormData();
        bodyFormData.append('id',id);
        bodyFormData.append('type',type);

        return fetch(this._apiBase+"disable-notification", {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            },
            body: bodyFormData,
        })
            .then(res => {
                if(res.status!==200) throw new Error("Введены неправильные данные. Повторите заново!");
                return res.json();
            })
            .then((res)=>{
                return res;
            });
    };

    addPartner = ( id, token, type ) => {
        return this.getResources(`addto${type}?id=`+id,token);
    };

    sendPost = ( id, ad, to, token ) => {
        console.log(id, ad, to, token);
        return this.getResources(`feeds?post=${id}&ad=${ad}&friends=${to}`,token);
    };

    addToFeed = ( id, token ) => {
        return this.getResources(`addtofeed?id=${id}`,token);
    };


    editProfile = ( form, token ) => {
        // console.log("cover:",form.cover)

        console.log("catssss:",form.cats)
        let cat = []
            form.cats.map((item,index)=>{
                console.log("item:",item)
                if(!isNaN(item)){
                    cat.push(item)
                }
            })
        // console.log("license:",form.license)
        console.log("cat:",cat)
        let bodyFormData = new FormData();
        const photo = form.photo?form.photo.split('/'):null;
        const photoCover = form.cover?form.cover.split('/'):null;
        const license = form.license?form.license.split('/'):null
        const cert = form.cert?form.cert.split('/'):null
        // const pass = tex_pass.path.split('/');


        // bodyFormData.append(`techPassportPhoto`, {
        //     name: tex_pass.name ? tex_pass.name : pass[pass.length - 1],
        //     filename: tex_pass.filename ? tex_pass.filename : pass[pass.length - 1],
        //     type: tex_pass.type ? tex_pass.type : tex_pass.mime,
        //     uri: tex_pass.sourceURL ? tex_pass.sourceURL : tex_pass.path
        // });
        bodyFormData.append("Profile[name]", `${form.name}`);
        bodyFormData.append("Profile[surname]", `${form.surname}`);
        bodyFormData.append("Profile[companyname]", `${form.companyname}`);
        bodyFormData.append("Profile[companytext]", `${form.companytext}`);
        bodyFormData.append("Profile[address]", `${form.address}`);
        bodyFormData.append("Profile[phone]", `${form.phone}`);
        bodyFormData.append("Profile[phone2]", `${form.phone2}`);
        bodyFormData.append("Profile[website]", `${form.website}`);
        bodyFormData.append("Profile[companylink]", `${form.companylink}`);
        bodyFormData.append(
            "Profile[cats]",
            JSON.stringify(
                cat
            )
        );

        if (form.photo) {
            bodyFormData.append(`Profile[photo]`, {
                name: form.photo.name ? form.photo.name : photo[photo.length - 1],
                filename: form.photo.filename ? form.photo.filename : photo[photo.length - 1],
                type: mime.getType( form.photo),
                uri: form.photo
            });

        }
        if (form.cover) {
            bodyFormData.append(`Profile[cover]`, {
                name: form.cover.name ? form.cover.name : photoCover[photoCover.length - 1],
                filename: form.cover.filename ? form.cover.filename : photoCover[photoCover.length - 1],
                type: mime.getType(form.cover),
                uri: form.cover
            });
        }
        if (form.license) {
            bodyFormData.append(`Profile[license]`, {
                name: form.license.name ? form.license.name : license[license.length - 1],
                filename: form.license.filename ? form.license.filename : license[license.length - 1],
                type: mime.getType(form.license),
                uri: form.license
            });
        }
        if (form.cert) {
            bodyFormData.append(`Profile[cert]`, {
                name: form.cert.name ? form.cert.name : cert[cert.length - 1],
                filename: form.cert.filename ? form.cert.filename : cert[cert.length - 1],
                type:mime.getType(form.cert),
                uri: form.cert
            });

        }
        console.log("Request:",bodyFormData)

        return fetch(this._apiBase +'edit',{
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            },
            body: bodyFormData,
        }).then(res=>{
            console.log("Request:",bodyFormData)
            console.log("Response:",res)

        }

        ).catch(err=>console.log(err))
    };

}
export default ApiService;
