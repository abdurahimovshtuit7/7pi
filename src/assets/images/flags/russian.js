import * as React from "react";
import Svg, { Circle, Path, G } from "react-native-svg";

const SvgComponent = (props) => {
    return (
        <Svg
            id="Layer_1"
            x="0px"
            y="0px"
            height={props.size}
            width={props.size}
            viewBox="0 0 512 512"
            xmlSpace="preserve"
            enableBackground="new 0 0 512 512"
            {...props}
        >
            <Circle cx={256} cy={256} r={256} fill="#f0f0f0" />
            <Path
                d="M496.077,345.043C506.368,317.31,512,287.314,512,256s-5.632-61.31-15.923-89.043H15.923 C5.633,194.69,0,224.686,0,256s5.633,61.31,15.923,89.043L256,367.304L496.077,345.043z"
                fill="#0052b4"
            />
            <Path
                d="M256,512c110.071,0,203.906-69.472,240.077-166.957H15.923C52.094,442.528,145.929,512,256,512z"
                fill="#d80027"
            />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
            <G />
        </Svg>
    );
}

export default SvgComponent;
