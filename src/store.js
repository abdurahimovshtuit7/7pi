import createSecureStore from "redux-persist-expo-securestore";
import ExpoFileSystemStorage from "redux-persist-expo-filesystem";

import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import reducers from "./reducers";

// Secure storage
const storage = createSecureStore();

const config = {
    key: "root",
    keyPrefix: '',
    storage: ExpoFileSystemStorage
};

const reducer = persistReducer(config, reducers);
const store = createStore(reducer);
const persistor = persistStore(store);

export { persistor, store };
