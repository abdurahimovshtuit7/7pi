import {
    categories,
    subcategories,
    company,
    shop,
    clients,
    providers,
    companies,
    product,
    products,
    user,
    feed,
    notifications, profile, interests, searchProducts, searchCompanies, search,
    PAGE,
} from "../constants/actionTypes";
import {get} from 'lodash'
import * as SecureStore from "expo-secure-store";
import i18next from 'i18next';
// const lang = get(i18next,'languages[0]','uz')


const itemsRequested = (type) => {
    switch (type) {
        case 'categories': return {
            type: categories.REQUEST
        };

        case 'subcategories': return {
            type: subcategories.REQUEST
        };

        case 'products': return {
            type: products.REQUEST
        };

        case 'companies': return {
            type: companies.REQUEST
        };

        case 'shop': return {
            type: shop.REQUEST
        };

        case 'clients': return {
            type: clients.REQUEST
        };

        case 'providers': return {
            type: providers.REQUEST
        };

        case 'interests': return {
            type: interests.REQUEST
        };

        case 'feed': return {
            type: feed.REQUEST
        };

        case 'notifications': return {
            type: notifications.REQUEST
        };

        case 'searchProducts': return {
            type: searchProducts.REQUEST
        };

        case 'searchCompanies': return {
            type: searchCompanies.REQUEST
        };

        default: return {
            type: categories.REQUEST
        };
    }
};

const itemsLoaded = (type,newItems) => {
    switch (type) {
        case 'categories': return {
            type: categories.SUCCESS,
            payload: newItems
        };

        case 'subcategories': return {
            type: subcategories.SUCCESS,
            payload: newItems
        };

        case 'products': return {
            type: products.SUCCESS,
            payload: newItems
        };

        case 'companies': return {
            type: companies.SUCCESS,
            payload: newItems
        };

        case 'shop': return {
            type: shop.SUCCESS,
            payload: newItems
        };

        case 'clients': return {
            type: clients.SUCCESS,
            payload: newItems
        };

        case 'providers': return {
            type: providers.SUCCESS,
            payload: newItems
        };

        case 'interests': return {
            type: interests.SUCCESS,
            payload: newItems
        };

        case 'feed': return {
            type: feed.SUCCESS,
            payload: newItems
        };

        case 'notifications': return {
            type: notifications.SUCCESS,
            payload: newItems
        };

        case 'searchProducts': return {
            type: searchProducts.SUCCESS,
            payload: newItems
        };

        case 'searchCompanies': return {
            type: searchCompanies.SUCCESS,
            payload: newItems
        };

        default: return {
            type: categories.SUCCESS,
            payload: newItems
        };
    }
};
export const Page = (page) => {
    return {
        type: PAGE,
        payload: page
    };
}

const itemsError = (type,error) => {
    switch (type) {
        case 'categories': return {
            type: categories.FAILURE,
            payload: error
        };

        case 'subcategories': return {
            type: subcategories.FAILURE,
            payload: error
        };

        case 'products': return {
            type: products.FAILURE,
            payload: error
        };

        case 'companies': return {
            type: companies.FAILURE,
            payload: error
        };

        case 'shop': return {
            type: shop.FAILURE,
            payload: error
        };

        case 'clients': return {
            type: clients.FAILURE,
            payload: error
        };

        case 'providers': return {
            type: providers.FAILURE,
            payload: error
        };

        case 'interests': return {
            type: interests.FAILURE,
            payload: error
        };

        case 'feed': return {
            type: feed.FAILURE,
            payload: error
        };

        case 'notifications': return {
            type: notifications.FAILURE,
            payload: error
        };

        case 'searchProducts': return {
            type: searchProducts.FAILURE,
            payload: error
        };

        case 'searchCompanies': return {
            type: searchCompanies.FAILURE,
            payload: error
        };

        default: return {
            type: categories.FAILURE,
            payload: error
        };
    }
};

const itemsFetch = ( apiService, dispatch, url, type, token ) => {
    const lang = get(i18next,'languages[0]','uz')
    dispatch(itemsRequested(type));
    if(type==="searchProducts"||type==="searchCompanies"||type==="feed"||type==="shop"){
        apiService.getResources(`${url}`,token)
            .then((data)=>{
                // let pg = page++
                // dispatch(Page(pg))
                dispatch(itemsLoaded(type,data))


            })
            .catch((err)=>dispatch(itemsError(type,err)));
    }
    apiService.getResources(`${url}?lang=${lang}`,token)
        .then((data)=>{
            // let p = page++
            //     dispatch(Page(p))
                dispatch(itemsLoaded(type,data),

                    // console.log("RESPONSE:",data)
        )}
        )
        .catch((err)=>dispatch(itemsError(type,err)));

};

const productRequested = () => {
    return {
        type: product.REQUEST
    };
};

const productLoaded = (newItem) => {
    return {
        type: product.SUCCESS,
        payload: newItem
    };
};
const changeLanguage = (lang) => {
     return {
         type:SET_LANGUAGE,
         lang,
     }
};

const productError = (error) => {
    return {
        type: product.FAILURE,
        payload: error
    };
};

const productFetch = ( apiService, dispatch, id ) => () => {
    const lang = get(i18next,'languages[0]','uz')
    dispatch(productRequested());
    apiService.getResources(`ad/${id}?lang=${lang}`)
        .then((data)=>dispatch(productLoaded(data)))
        .catch((err)=>dispatch(productError(err)));

};

const companyRequested = () => {
    return {
        type: company.REQUEST
    };
};

const companyLoaded = (newItem) => {
    return {
        type: company.SUCCESS,
        payload: newItem
    };
};

const companyError = (error) => {
    return {
        type: company.FAILURE,
        payload: error
    };
};

const profileRequested = () => {
    return {
        type: profile.REQUEST
    };
};

const profileLoaded = (newItem) => {
    return {
        type: profile.SUCCESS,
        payload: newItem
    };
};

const profileError = (error) => {
    return {
        type: profile.FAILURE,
        payload: error
    };
};

const profileFetch = ( apiService, dispatch, token ) => {
    const lang = get(i18next,'languages[0]','uz')
    dispatch(profileRequested());
    apiService.getResources(`info?lang=${lang}`,token)
        .then((data)=>dispatch(profileLoaded(data.profile)))
        .catch((err)=>dispatch(profileError(err)));

};

const companyFetch = ( apiService, dispatch, id ) => () => {
    const lang = get(i18next,'languages[0]','uz')
    dispatch(companyRequested());
    apiService.getResources(`profile-link?id=${id}&lang=${lang}`)
        .then((data)=>dispatch(companyLoaded(data)))
        .catch((err)=>dispatch(companyError(err)));

};
const loginRequested = () => {
    return {
        type: user.LOGIN_REQUEST
    };
};

const loginLoaded = (data) => {
    return {
        type: user.LOGIN_SUCCESS,
        payload: data

    };
};

const logout = () => {
    return {
        type: user.LOGOUT
    };
};

const loginError = (data) => {
    return {
        type: user.LOGIN_FAILURE,
        payload: data
    };
};

const login = ( apiService, dispatch, form ) => {
    const lang = get(i18next,'languages[0]','uz')

    dispatch(loginRequested());
    apiService.login(form)
        .then((data)=>dispatch(loginLoaded(data)))
        .catch((error)=>{
            alert(error.message);
            dispatch(loginError(error.message));
        });

};

const signupRequested = () => {
    return {
        type: user.REGISTER_REQUEST
    };
};

const signupLoaded = (data) => {
    return {
        type: user.REGISTER_SUCCESS,
        payload: data

    };
};

const signupError = (data) => {
    return {
        type: user.REGISTER_FAILURE,
        payload: data
    };
};

const feedUpdate = (data) => {
    return {
        type: feed.UPDATE,
        payload: data
    };
};

const commentsUpdate = (data) => {
    return {
        type: feed.UPDATE_COMMENTS,
        payload: data
    };
};

const companySelect = (data) => {
    return {
        type: companies.SELECT,
        payload: data
    };
};

const productSelect = (data) => {
    return {
        type: product.SELECT,
        payload: data
    };
};

const signup = ( apiService, dispatch, form ) => {
    console.log("signUP_API:",apiService,dispatch,form)
    dispatch(signupRequested());
    apiService.register(form)
        .then((data)=>{
            dispatch(signupLoaded(data))
        })
        .catch((error)=>{
            alert(error.message);
            dispatch(signupError(error.message));
        });

};

const searchUpdate = (data) => {
    return {
        type: search.UPDATE,
        payload: data
    };
};

const addProvider = (apiService, dispatch, id, token) => {
    const lang = get(i18next,'languages[0]','uz')

    apiService.addPartner(id,token,`provider`).then(res=>{
        dispatch({
            type: profile.ADD_PROVIDER,
            payload: id
        });
    });
};

const addClient = (apiService, dispatch, id, token) => {
    apiService.addPartner(id,token,'client').then(res=>{
        dispatch({
            type: profile.ADD_CLIENT,
            payload: id
        });
    });
};

export {
    itemsFetch,
    productFetch,
    companyFetch,
    profileFetch,
    login,
    logout,
    signup,
    productSelect,
    companySelect,
    searchUpdate,
    addProvider,
    addClient,
    feedUpdate,
    commentsUpdate,
    changeLanguage
};
