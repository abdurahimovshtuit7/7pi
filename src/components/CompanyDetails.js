import React, {useState, useEffect} from 'react';
import styled from "styled-components/native";
import {withNavigation} from "react-navigation";
import {LinearGradient} from 'expo-linear-gradient';
import R from "../constants/R";
import Detail from "./Detail";
import Icon from "../constants/Icons";
import Button from "./button/Button";
import {addClient, addProvider, itemsFetch} from "../actions";
import {useDispatch, useSelector, connect} from "react-redux";
import withApiService from "./hoc";
import {withTranslation} from "react-i18next";
import {Linking} from "react-native";
import converter from "../services/converter";
import translate from "translate-google-api";
import {get} from "lodash";
import i18next from "i18next";
// import connect from "react-redux";

const CompanyDetails = ({item, navigation, apiService, t, language}) => {

    const {user, nik, companyname, name, surname, companytext, catNames, companylink, email, phone, phone2, address, website, photo, cover, cert, license} = item;
    const [textLang, setText] = useState("");
    const [activity, setActivity] = useState([]);
    const lang = get(i18next, 'languages[0]', 'uz')
    const token = useSelector(state => state.auth.token);
    const profile = useSelector(state => state.user);
    const currentLangCode = useSelector(state => state.currentLangCode)
    const dispatch = useDispatch();
    console.log("ITEM:", item)
    const getButtons = () => {
        if (!profile || !profile.providers || !profile.clients) return null;
        if (Object.values(profile.providers).indexOf(user) > -1) {
            return <Col><Button text="Поставщик" icon="check" flex={true} type="link" disabled={true}/></Col>
        } else if (Object.values(profile.clients).indexOf(user) > -1) {
            return <Col><Button text="Клиент" icon="check" flex={true} type="link" disabled={true}/></Col>
        } else {
            return <Col>
                <Button text={t("supplier")} icon="plus" flex={true} type="link"
                        onPress={() => addProvider(apiService, dispatch, user, token)}/>
                <Button text={t("client")} icon="plus" flex={true} type="link"
                        onPress={() => addClient(apiService, dispatch, user, token)}/>
            </Col>
        }
    };
    useEffect(() => {
        const lang = get(i18next, 'languages[0]', 'uz')
        // console.log("currentLangCode:",lang)
        translateAsync(companytext, lang)
        // translateAsync1(item, lang)
    }, [lang])
    const remove = () => {
        apiService.removePartner(user, token).then(res => {

        });
    };
    const translateAsync = (text, language) => {
        let l
        if (language === "oz" || language === "uz") {
            l = "uz"
        } else {
            l = language
        }
        translate(text, {
            tld: "ru",
            to: l,
        }).then(res => {
            console.log("RES:", res[0])
            let t
            if (language === "uz") {
                t = converter(res[0])
            } else {
                t = res[0]
            }
            setText(t)

        }).catch(err => {
            console.error(err)
        });
    }
     const translateAsync1 = (array, language) => {
        let l
        if (language === "oz" || language === "uz") {
            l = "uz"
        } else {
            l = language
        }
        console.log("ARR:",array.catNames)
        // array.catNames.map((item,index)=>{
        translate(array.catNames, {
            tld: "ru",
            to: l,
            // proxy: {
            //     host: '127.0.0.1',
            //     port: 9000,
            //     auth: {
            //         username: 'abdurahimovshtuit@gmail.com',
            //         password: '11017400Shax'
            //     }}

        }).then(res => {
            console.log("RES:", res)
            let t
            // if (language === "uz") {
            //     t = converter(res[0])
            // } else {
                t = res
            // }
            setActivity([t]);

        }).catch(err => {
            console.error(err)
        });
    // })
        // return activity
    }
    console.log("Activity:",activity)
    return <Container showsVerticalScrollIndicator={false}>
        <Image source={{uri: cover}}>
        </Image>
        <Overlay
            locations={[0, 1.0]}
            colors={['rgba(0,0,0,0.00)', 'rgba(0,0,0,0.6)']}>
            <Logo source={{uri: photo}} resizeMode="contain"/>
            <Links>
                <Link>
                    <Icon name="info" width={20} height={20} fill={R.colors.gold}/>
                    <LinkText gold>{t("information")}</LinkText>
                    <Icon name="caret" width={15} height={15} fill={R.colors.gold}/>
                </Link>
                <Link onPress={() => navigation.navigate('Shop', {id: user})}>
                    <Icon name="shop" width={20} height={20} fill={R.colors.white}/>
                    <LinkText>{t("market")}</LinkText>
                    <Icon name="caret" width={15} height={15} fill={R.colors.white}/>
                </Link>
                <Link onPress={() => {
                    Linking.openURL(website?website:"https://7pi.uz/").then(r => console.log(r))
                }}>
                    <Icon name="globe" width={20} height={20} fill={R.colors.white}/>
                    <LinkText>{t("web")}</LinkText>
                    <Icon name="caret" width={15} height={15} fill={R.colors.white}/>
                </Link>
                {/*{getButtons()}*/}
            </Links>
            <View>
                {
                  (Object.values(profile.providers).indexOf(user) > -1)?
                      (
                         <Button text="Поставщик" icon="check"  type="" disabled={true}/>
                      ) :((Object.values(profile.clients).indexOf(user) > -1)?(
                              <Button text="Клиент" icon="check"  type="" disabled={true}/>
                          ):null
                      )
                }

            </View>
        </Overlay>
        <Box>
            <Title selectable>{nik}</Title>
            <Line/>

            <Detail item={t("company")} value={companyname}/>
            <Detail item={t("menejer")} value={name + ' ' + surname}/>
            <Detail item={t("hello")} type={1} value={item.catNames.map((item,index)=>{
                return (`${item}\n`)

            })}>

            </Detail>
            <Detail   item={t("email")} value={email}/>
            <Detail item={t("link")} modal={t("modalText")} type={2} value={companylink} url={"2"}/>
            <Detail item={t("web")} value={website} url={"1"}/>
            <Detail item={t("phone")} value={phone + ' ' + phone2}/>
            <Detail item={t("address")} value={address}/>
        </Box>
        <Company>
            <Title>{t("about_company")}</Title>
            <Line/>
            <Desc>{textLang}</Desc>
        </Company>
        <Company>
            <Title>{t("document")}</Title>
            <Line/>
            <Docs>
                {cert && <Doc source={{uri: cert}} resizeMode="contain"/>}
                {license && <Doc source={{uri: license}} resizeMode="contain"/>}
            </Docs>
        </Company>
    </Container>
};

const Container = styled.ScrollView`
    margin: 16px 16px 0px 16px;
`;
const Image = styled.ImageBackground`
    background-color: #FFF;
    position: relative;
    padding: 20px;
    width: 100%;
    height: 220px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;
const Title = styled.Text`
    color: #323232;
    font-family: LatoBold;
    margin-bottom: 4px;
    font-size: 20px;
`;
const LinkText = styled.Text`
    color: ${props => props.gold ? R.colors.gold : R.colors.white};
    font-family: LatoBold;
    margin-left: 8px;
    margin-right: 3px;
    font-size: 16px;
`;
const Link = styled.TouchableOpacity`
    padding: 12px 12px 0px 0px;
    align-items: center;
    flex-direction: row;
`;
const Links = styled.View`
    position: absolute;
    bottom: 16px;
    left: 16px;
`;
const Line = styled.Text`
    height: 1.5px;
    width: 50px;
    margin-bottom: 12px;
    background: #003e7a;
`;
const Overlay = styled(LinearGradient)`
    position:absolute; 
    width: ${R.strings.width - 32}; 
    height: 220px;
    z-index: 2;
    padding: 16px;
`;
const Desc = styled.Text`
    color: #323232;
    font-family: Lato;
    margin-bottom: 8px;
    font-size: 16px;
    line-height: 22px;
`;
const Box = styled.View`
    margin: 16px 0px;
    background: #fff;
    padding: 15px;
    box-shadow: 5px 5px 5px rgba(0,0,0,.1);
    border-radius: 6px;
`;
const Company = styled.View`
    margin: 0px 0px 16px 0px;
    background: #fff;
    padding: 15px;
    box-shadow: 5px 5px 5px rgba(0,0,0,.1);
    border-radius: 6px;
`;

const Logo = styled.Image`
    width: 60px;
    height: 60px;
    border-radius: 6px;
    background-color:#FFF;;
`;
const Docs = styled.View`
  flex-direction: row;
`;
const Doc = styled.Image`
   height: 200px;
   margin-right: 16px;
   width: ${R.strings.width / 2 - 64}px;
`;
const Col = styled.View`
    flex-direction: row;
    margin-left: -6px;
    margin-right: -6px;
    margin-bottom: 12px;
`;
const View =styled.View`
   position:absolute;
   bottom:10;
   right:10;
`
const Text = styled.Text`
    padding-horizontal:20;
    padding-vertical:7;
    color:#fff;
`
export default withApiService()(withTranslation('main')(withNavigation(CompanyDetails)));
