import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import R from '../../constants/R'
import {withTranslation} from "react-i18next";

const ErrorIndicator = (error) =>{

    return <View style={styles.box}>
        <Image style={styles.image} source={R.images.logo} resizeMode="contain"/>
        <Text style={styles.text}>{error.t("errorMessage")}</Text>
    </View>
};
export default withTranslation('main')(ErrorIndicator);

const styles = StyleSheet.create({
    box:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    image: {
        marginBottom: 20,
        width: 100,
        height: 100
    },
    text:{
      textAlign: 'center'
    }
});
