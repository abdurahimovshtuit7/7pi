import React, { useState } from "react";
import styled from "styled-components/native";
import Icon from "../constants/Icons";
import Colors from "../constants/Colors";
import { TextInput } from "react-native";
import {useDispatch} from "react-redux";
import {commentsUpdate} from "../actions";
import {withTranslation} from "react-i18next";

const CommentInput = ({ uri, user, ad, answer, api, token,t }) => {

    const [ text, setText ] = useState(null);
    const id = user ? "po" + ad : "ad" + ad;
    const dispatch = useDispatch();

    const addComment = () => {

        if(text===null || text.length<1) return;
        api.addComment(id,text,answer,token).then(res=>{
            setText(null);
            dispatch(commentsUpdate(id));
        })
    };

    return <Col>
        <Photo source={{uri}}/>
        <Input
            value={text}
            onChangeText={setText}
            multiline={true}
            placeholder={t("leave_comment")}/>
        <Link onPress={addComment}><Icon name="send" width={24} height={24} fill={Colors.tintColor}/></Link>
    </Col>
};

const Col = styled.View`
    margin: 10px;
    background: ${Colors.white};
    flex-direction: row;
    align-items: center;
    box-shadow: 0 2px 15px rgba(0,0,0,.25);
    border-radius: 5px;
`;

const Input = styled(TextInput)`
    flex: 1;
    padding: 14px 6px 12px;
    font-size: 16px;
    font-family: "Lato";
    color: ${Colors.text};
`;

const Ico = styled(Icon)`
    background: ${Colors.white};
    margin: 16px;
`;

const Link = styled.TouchableOpacity`

`;

const Photo = styled.Image`
    width: 40px;
    height: 40px;
`;

export default withTranslation('main')(CommentInput);
