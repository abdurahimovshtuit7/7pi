import React, { useState } from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import Colors from "../constants/Colors";
import Button from "./button/Button";
import Icon from "../constants/Icons";
import { Linking } from 'expo';
import { useSelector } from "react-redux";
import { Alert, Modal, SafeAreaView } from "react-native";
import ImageViewer from 'react-native-image-zoom-viewer';
import CommentInput from "./CommentInput";
import CommentListContainer from "../containers/item-list-container/CommentListContainer";
import {withTranslation} from "react-i18next";

const Post = ({ api, item, setRefresh, navigation,t }) => {

  const { user, author, id, text, userphoto, username, bigPhoto, title, address, phone, email, priceName, commentnum } = item;

  const [ visible, setVisible ] = useState(false);

  const [ actions, setActions ] = useState(false);

  const [ zoom, setZoom ] = useState(false);

  const [ more, setMore ] = useState(false);

  const profile = useSelector(state => state.user);

  const token = useSelector(state => state.auth.token);
  const updateComments = useSelector(state => state.updateComments);

  const open = (url) => {
    Linking.openURL(url);
  };

  const send = () => {
    Alert.alert(t("send_message"),null, [
      {text: t("clients"), onPress: () => navigation.push('Clients',{action:"send",params:{id,ad:0}})},
      {text: t("suppliers"), onPress: () => navigation.push('Providers',{action:"send",params:{id,ad:0}})},
      {text: t("tender"), onPress: () => navigation.navigate('SelectCategory',{next:'companies',action:"send",params:{id,ad:0}})},
    ], {cancelable: true});
  };

  const remove = () => {
    let part = "";

    user && user === profile.id
        ? (part = "remove-post")
        : (part = "remove-shared");

    api.getResources(part+"?id="+id,token).then(res=>setRefresh(new Date()));
  };

  const getCommentForm = () => {
    if(user === profile.id){
      return null;
    }else{
      return <CommentInput uri={profile.photo} user={user} token={token} answer={0} ad={id} api={api}/>
    }
  };

  return <Container onPress={()=>navigation.navigate('CompanyDetails',{id:user?user:author})}>
    <Logo source={{uri:userphoto}} resizeMode="contain" />
    <Username>{username}</Username>
    {title && <Title>{title}</Title>}
    {priceName && <Price>{priceName}</Price>}
    <Text more={more}>{text}</Text>
    <ShowMore onPress={()=>setMore(!more)}><ShowMoreText>полностью <Icon name="down" width={16} height={16} fill={Colors.blue}/></ShowMoreText></ShowMore>
    <Close onPress={()=>setVisible(!visible)} onBlur={()=>setVisible(false)}><Icon name="close" width={16} height={16}/></Close>

    {visible && <Actions>
      <Link><Label>{t("close_topic")}</Label></Link>
      <Link onPress={remove}><Label>{t("delete_forever")}</Label></Link>
    </Actions>}

    {user === profile.id && <Col>
      <Button flex={true} type="green-inline" onPress={()=>navigation.navigate('EditPost',{id:id,setRefresh})} text={t("edit")}/>
      <Button flex={true} type="primary-inline" onPress={send} text={t("send_message")}/>
    </Col>}
    {phone && <Contacts>
      <Contact onPress={()=>open(`https://maps.google.com/?q=`+address)}><Icon name="location" width={20} height={20} fill={Colors.blue}/></Contact>
      <Contact onPress={()=>open(`tel:`+phone)}><Icon name="call" width={20} height={20} fill={Colors.blue}/></Contact>
      <Contact onPress={()=>open(`mailto:`+email)}><Icon name="message" width={20} height={20} fill={Colors.blue}/></Contact>
    </Contacts>}
    {bigPhoto &&<>
    <Link onPress={()=>setZoom(true)}><Image source={{uri:bigPhoto}} /></Link>
      <Modal
          animationType="slide"
          transparent={false}
          visible={zoom}>
        <SafeAreaView style={{flex: 1, position:"relative", backgroundColor: "#000000"}}>
          <ZoomClose onPress={()=>setZoom(false)}><Icon name="close" width={16} height={16} fill={Colors.white}/></ZoomClose>
          <ImageViewer imageUrls={[{url:bigPhoto}]}/>
        </SafeAreaView>
      </Modal>
    </>}
    {parseInt(commentnum)>0 && <CommentListContainer ad={id} user={user} navigation={navigation} token={token} updateComments={updateComments}/>}
    {getCommentForm()}
  </Container>
};

const Container = styled.TouchableOpacity`
    margin: 64px 16px 0px 16px;
    position: relative;
    border-radius: 8px;
    background-color: #fff;
    overflow: visible;
`;
const Actions = styled.View`
    position: absolute;
    top: 44px;
    right: 12px;
    box-shadow: 0 2px 10px rgba(0,0,0,.15);
    border-radius: 5px;
    background-color: #fff;
`;
const Link = styled.TouchableOpacity`

`;
const Label = styled.Text`
    color: #2d96ff;
    font-family: Lato;
    font-size: 18px;
    padding: 10px;
`;
const Image = styled.ImageBackground`
    background-color: #FFF;
    position: relative;
    padding: 20px;
    width: 100%;
    height: 240px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;
const Username = styled.Text`
    color: ${Colors.blue};
    font-family: LatoBold;
    padding: 8px;
    margin-bottom: 0px;
    margin-top: -30px;
    font-size: 20px;
`;
const Title = styled.Text`
    color: ${Colors.text};
    font-family: LatoBold;
    padding: 8px;
    font-size: 20px;
`;
const ShowMore = styled.TouchableOpacity`
    margin: 0px 16px 16px 8px;
`;
const Close = styled.TouchableOpacity`
    position: absolute;
    top: 16px;
    right: 16px;
    z-index: 2;
    padding:13px;
`;

const ZoomClose = styled(Close)`
   top: 36px;
   right: 36px;
`;

const ShowMoreText = styled.Text`
    color: ${Colors.blue};
    font-family: LatoMedium;
    font-size: 16px;
`;
const TitleBox = styled.TouchableOpacity`
    border-bottom-color: #e0e6ec;
    border-bottom-width: 1px;
`;
const Text = styled.Text`
    padding: 8px;
    color: #323232;
    ${({ more }) => !more?"height:86px;":""};
    font-family: Lato;
    font-size: 14px;
`;
const Price = styled.Text`
    padding: 0px 8px;
    color: ${Colors.blue};
    font-family: LatoMedium;
    font-size: 20px;
`;
const Col = styled.View`
    flex-direction: row;
    margin: 0px 8px;
    margin-bottom: 12px;
    overflow: visible;
`;
const Contact = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
`;
const Contacts = styled.View`
    padding: 8px;
    background-color: #e0e6ec;
    flex-direction: row;
    align-items: center;
`;
const Logo = styled.Image`
    width: 60px;
    height: 60px;
    border-radius: 6px;
    background-color:#FFF;
    border-width: 1px;
    border-color: #ddd;
    position: relative;
    top: -30px;
    left: 16px;
    box-shadow: 20px 10px 50px rgba(0,0,0,.15);
`;
export default withTranslation('main')(withNavigation(Post));
