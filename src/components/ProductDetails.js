import React, { useState, useEffect } from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import { LinearGradient } from 'expo-linear-gradient';
import R from "../constants/R";
import Button from "./button/Button";
import Icon from "../constants/Icons";
import {feedUpdate, profileFetch} from "../actions";
import withApiService from "./hoc";
import {useDispatch, useSelector} from "react-redux";
import {withTranslation} from "react-i18next";
import {connect} from 'react-redux';
import translate from "translate-google-api";
import converter from "../services/converter";


const ProductDetails = ({item,onPress=()=>{},navigation, apiService,t,currentLangCode}) => {

  const { id, bigPhoto, title, address, company, priceName, text, phonecode, phone } = item;

  const [ imgRatio, setImgRatio ] = useState(1.776470588235294)
  const [textLang, setText] = useState("");
  const [ isFeed, setIsFeed ] = useState(false);
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();

  useEffect(()=>{
    Image.getSize(bigPhoto, (width, height)=>{
      setImgRatio(width/height)
    },(err)=>console.log(err))
    translateAsync(text, currentLangCode)
  },[currentLangCode])

  const addFeed = () => {
    setIsFeed(true);
    dispatch(feedUpdate(new Date()));
    apiService.addToFeed(id,token);
  };
  const translateAsync = (text, language) => {
    let l
    if (language === "oz" || language === "uz") {
      l = "uz"
    } else {
      l = language
    }
    translate(text, {
      tld: "ru",
      to: l,
    }).then(res => {
      console.log("RES:", res[0])
      let t
      if (language === "uz") {
        t = converter(res[0])
      } else {
        t = res[0]
      }
      setText(t)

    }).catch(err => {
      console.error(err)
    });
  }

  return <Container showsVerticalScrollIndicator={false}>
    <Image source={{uri:bigPhoto}} ratio={imgRatio} resizeMode="contain">
    </Image>
    <Overlay
        locations={[0, 1.0]}
        colors={['rgba(0,0,0,0)', 'rgba(0,0,0,0)']}>
      {
        isFeed?<Button text={t("lentada")} type="primary" icon="check" flex={true}/>:
            <Button text={t("addToFeed")} type="primary" onPress={addFeed}/>
      }
    </Overlay>
    <Box>
      <Title>{title}</Title>
      <Desc>{textLang}</Desc>
      <Price>{priceName}</Price>
      <Touch><Icon name="location" width={16} height={16} fill={R.colors.blue}/><Address> {address}</Address></Touch>
      <Touch><Icon name="call" width={16} height={16} fill={R.colors.blue}/><Address> +{phonecode} {phone}</Address></Touch>
    </Box>
    <Company onPress={()=>navigation.navigate('CompanyDetails',{id:company.user})}>
      <Logo source={{uri:company.photo}} resizeMode="contain"/>
      <NameBox>
        <Label>{t("company_store")}</Label>
      <CompanyName>{company.nik}</CompanyName>
      </NameBox>
      <Icon name="caret" fill={R.colors.blue} width={24} height={24} />
    </Company>
  </Container>
};

const Container = styled.ScrollView`
    margin: 16px 16px 0 16px;
`;
const Image = styled.Image`
    background-color: #FFF;
    width: ${R.strings.width-20};
    height: ${({ratio})=>(R.strings.width-20)/ratio};
    max-height: 240px;
`;
const Overlay = styled(LinearGradient)`
    width: ${R.strings.width-20};
    margin: 12px -6px 0 -6px;
    justify-content: flex-end;
    z-index: 2;
`;
const Title = styled.Text`
    color: #323232;
    font-family: LatoBold;
    margin-bottom: 8px;
    font-size: 20px;
`;
const Desc = styled.Text`
    color: #323232;
    font-family: Lato;
    margin-bottom: 8px;
    font-size: 16px;
    line-height: 22px;
`;
const Address = styled.Text`
    color: #323232;
    font-family: Lato;
    font-size: 16px;
    margin-right: 32px;
    margin-bottom: 12px;
`;
const Touch = styled.TouchableOpacity`
    flex-direction: row;
    align-items: center;
`;
const Price = styled.Text`
    color: #463232;
    padding: 12px 16px;
    text-align: center;
    border-color: #FFCB41;
    border-width: 2px;
    border-radius: 24px;
    font-family: LatoBold;
    font-size: 16px;
    margin-bottom: 12px;
`;
const Box = styled.View`
    margin: 16px 0px;
    background: #fff;
    padding: 15px;
    box-shadow: 5px 5px 5px rgba(0,0,0,.1);
    border-radius: 6px;
`;
const Company = styled.TouchableOpacity`
    margin: 0px 0px 16px 0px;
    background: #fff;
    padding: 10px;
    box-shadow: 5px 5px 5px rgba(0,0,0,.1);
    flex-direction: row;
    align-items: center;
    border-radius: 6px;
`;

const Logo = styled.Image`
    width: 50px;
    height: 50px;
    background-color:#FFF;;
`;
const NameBox = styled.View`
    flex: 1;
`;
const Label = styled.Text`
    color: ${R.colors.blue}
    text-transform: uppercase;
    margin-bottom: 4px;
    text-align: center;
    font-family: LatoBold;
`;
const CompanyName = styled.Text`
    text-align: center;
    text-transform: uppercase;
    font-family: LatoBold;
`;
const mapStateToProps = (state, ownProps) => {
  return {
    currentLangCode: state.lang,
  };
};
export default connect(mapStateToProps)(withNavigation(withApiService()(withTranslation('main')(ProductDetails))));
