import React, { useState, useEffect } from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import { LinearGradient } from 'expo-linear-gradient';
import R from "../constants/R";
import Button from "./button/Button";
import Icon from "../constants/Icons";
import {connect, useDispatch, useSelector} from "react-redux";
import {feedUpdate, itemsFetch, productSelect} from "../actions";
import {withTranslation} from "react-i18next";
import {Alert, StyleSheet, Text, TouchableOpacity} from "react-native";
import withApiService from "./hoc";
import Modal from "react-native-modal";
import colors from '../constants/Colors'
import UK from "../assets/images/flags/uk";
import RU from "../assets/images/flags/russian";
import UZ from "../assets/images/flags/uzbek";

const Product = ({ apiService,item, navigation, pop, api, action=null,t ,currentLangCode}) => {
  const _apiBase = 'https://7pi.uz/rest/';
  const { photo, bigPhoto, title, cityName, id, priceName, company } = item;

  const [ isFeed, setIsFeed ] = useState(false);

  const [ actionButton, setActionButton ] = useState(null);

  const profile = useSelector(state => state.user);
  const token = useSelector(state => state.auth.token);
  const selectedProduct = useSelector(state => state.selectedProduct);
  const dispatch = useDispatch();



  const navigate = () => {
    if(pop){
      navigation.push('ProductDetails',{id,date:new Date()});
    }else{
      navigation.navigate('ProductDetails',{id})
    }
  };

  const addFeed = () => {
    setIsFeed(true);
    dispatch(feedUpdate(new Date()));
    api.addToFeed(id,token);
  };
  const deleteModal = () => {

  }

  const [showModal, setShowModal] = useState(false);
  // const {t} = props

  const toggleModal = (id) => {
    setShowModal(!showModal)
    // Alert.alert(
    //     null,
    //     t("udalit"),
    //     [
    //       {
    //         text: 'No',
    //         onPress: () => console.log('Cancel Pressed'),
    //         style: 'cancel'
    //       },
    //       { text: 'Yes', onPress: () => { removeAd(id)} }
    //     ],
    //     { cancelable: false }
    // );
  }
  const removeAd = (id) => {

    // apiService.removeAd(id, token).then(r => console.log("res"))
    const url = _apiBase +`remove-ad?id=${id}`
    console.log("URL:",url)
    // const data = { id: id };
    return fetch(url,{
      headers: {
        // 'Content-Type': 'application/json',
        // 'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      },
    }).then(res =>{
      itemsFetch(apiService,dispatch,'shop',"shop",token);
      // setProgress(false)
      // console.log("res:",res)
      // Alert.alert(res,null,[],{cancelable: true});
      // navigation.navigate("MyShop");
    }).catch(err=>console.log(err))
  }

  useEffect(()=>{
    console.log("Product:",currentLangCode)

    // useEffect(()=>{

    // },[])

    if(profile?.shareds && Object.values(profile.shareds).indexOf(id)>-1){
      setIsFeed(true);
    }else{
      setIsFeed(false);
    }
    switch (action) {
      case "delete": setActionButton(<Delete onPress={()=>{
        // console.log("R:",item.id)
        toggleModal()
        // removeAd(item.id).then(r => console.log("a"))
      }}><Icon name="close" width={20} height={20}/></Delete>); break;
      case "send": setActionButton(<Send><Icon name="send" width={20} height={20}/></Send>); break;
      default: setActionButton(null);
    }
  },[currentLangCode]);
 // console.log("item:",item.id)
  return <Touch onPress={()=>selectedProduct===id?navigate():dispatch(productSelect(id))}>
    <Modal isVisible={showModal}
           onBackdropPress = {toggleModal}
           testID={'modal'}
           animationIn="slideInLeft"
           animationOut="slideOutRight"

    >
      <View style={styles.modal}>
        <Text style={styles.attention}>
          {t("attention")}
        </Text>
        <Text style={styles.label}>
          {t('udalit')}
        </Text>
        <TouchableOpacity style={[styles.button,{backgroundColor:colors.green}]}
                          onPress={() => {
                            removeAd(item.id)
                            toggleModal()
                          }}
        >
          {/*<UK size={30}/>*/}
          <Text style={styles.text}>
            {t("yes")}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button,{backgroundColor:colors.blue}]}
                          onPress={() => {
                            // removeAd(item.id)
                            toggleModal()
                          }}
        >
          {/*<RU size={30}/>*/}
          <Text style={styles.text}>
            {t("no")}
          </Text>
        </TouchableOpacity>

        {/*<Button title="Hide modal" onPress={toggleModal} />*/}
      </View>
    </Modal>
    <Image source={{uri:bigPhoto}}>

      {
        selectedProduct===id?
          <Overlay
              locations={[0, 1.0]}
              colors={['rgba(0,0,0,0.7)', 'rgba(0,0,0,0.7)']}>

              <Inits>
                <Logo source={{uri:company.photo}} resizeMode="contain"/>
                <CompanyName>{company.nik}</CompanyName>
                {actionButton}
              </Inits>



            <Buttons>
              {!isFeed?
                  <Button text={t('lenta')} icon="plus" flex={true} onPress={addFeed}/>:
                  <Button text={t("lentada")} icon="check" flex={true}/>}
              {action==="delete"?
                  <Button text={t("edit")} onPress={()=>navigation.navigate('AdForm',{ad:id})} flex={true}/>
                  :<Button text={t("more_details")} iconRight="caret" onPress={navigate} flex={true}/>}
            </Buttons>
          </Overlay>
          :
          <>
            <LinearGradient
                locations={[0, 1.0]}
                colors={['rgba(0,0,0,0.00)', 'rgba(0,0,0,0.5)']}
                style={{ position:'absolute', width:R.strings.width-32, height:240}}>
            </LinearGradient>
            <Col>
              <Title>{title}</Title>
              <Address><Icon name="location" width={16} height={16} fill={R.colors.white}/> {cityName}</Address>
            </Col>
            <Price>{priceName},</Price>
          </>
      }
    </Image>
  </Touch>
};

const Touch = styled.TouchableOpacity`
    margin: 16px 16px 0px 16px;
    height: 240px;
`;
const Image = styled.ImageBackground`
    background-color: #FFF;
    position: relative;
    padding: 12px;
    width: 100%; 
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;
const Col = styled.View`
    flex: 1;
`;
const Overlay = styled(LinearGradient)`
    position: absolute; 
    width: ${R.strings.width-32}; 
    height: 240px;
    z-index: 2;
`;
const Buttons = styled.View`
    position: absolute;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    padding: 4px;
    bottom: 0px;
`;
const View =styled.View`
flex-direction:row;
justify-content:space-between
`
const Inits = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 12px;
`;
const Title = styled.Text`
    color: #fff;
    font-family: LatoBold;
    margin-bottom: 8px;
    font-size: 18px;
`;
const CompanyName = styled.Text`
    color: #fff;
    flex: 1;
    font-family: LatoBold;
    margin-bottom: 8px;
    font-size: 20px;
`;
const Logo = styled.Image`
    width: 40px;
    height: 40px;
    margin-right: 12px;
    background: #FFFFFF;
`;
const Address = styled.Text`
    color: #fff;
    font-family: Lato;
    font-size: 14px;
`;
const Price = styled.Text`
    color: #ffffff;
    padding: 6px 16px;
    text-align: center;
    border-color: #FFCB41;
    border-width: 1px;
    border-radius: 6px;
    font-family: LatoBold;
    font-size: 14px;
`;
const Delete = styled.TouchableOpacity`
    padding: 6px;
    right: 5px;
    z-index:2;
    background-color: #ffffff;
    border-radius: 16px;
    border-width:1;
`;
const Send = styled.TouchableOpacity`
    padding: 6px;
`;
const styles = StyleSheet.create({
  modal: {
    backgroundColor: '#fff',
    paddingVertical: 30,
    // height: "50%",
    flexDirection: 'column',
    // justifyContent:'center',
    // alignItems: 'center',
    borderRadius:10,
    marginHorizontal:15
  },
  text: {
    color: '#fff',
    // width:'85%',
    // flex:0.5,
    fontSize:18,
    textAlign:'center',
    // paddingHorizontal:60,
    // borderWidth: 1
  },
  label:{
    paddingVertical:10,
    textAlign: 'center',
  },
  attention:{
    fontSize: 20,
    fontWeight:'bold',
    textAlign: 'center',
  },
  touchable:{
    // marginRight:10,
    // borderWidth:1,
    paddingRight:10,
    paddingVertical:7
  },
  button:{
    paddingHorizontal:20,
    marginHorizontal: 30,
    // flexDirection:'row',
    // justifyContent:'center',
    // alignItems: 'center',
    // borderWidth:1,
    paddingVertical:15,
    marginVertical:8,
    borderRadius: 5,


  }
})
const mapStateToProps = (state, ownProps) => {
  return {
    currentLangCode: state.lang,
  };
};
export default withApiService()(connect(mapStateToProps)(withTranslation('main')(withNavigation(Product))));
