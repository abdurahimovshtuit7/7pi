import React, { useState, useEffect } from "react"
import {useSelector, useDispatch, connect} from "react-redux";
import { View, ScrollView, FlatList, SafeAreaView } from "react-native"
import Tab from './Tab'
import Colors from "../constants/Colors";
import {user} from "../constants/actionTypes";
import {withTranslation} from "react-i18next";

import {itemsFetch, logout} from "../actions";
import { persistor } from "../store";



let TabBar = ({t,navigation,currentLangCode }) => {
    // const {t} = props
    // const k =  t("store")
    let DATA = [
        {
            id: 0,
            title:t("store"), //'Рынок'
            icon: 'market',
            route: 'Home'
        },
        {
            id: 1,
            title: t('companies'),
            icon: 'companies',
            route: 'Companies'
        },
        {
            id: 2,
            title: t('tape'),
            icon: 'feed',
            route: 'Feed'
        },
        {
            id: 3,
            title: t('clients'),
            icon: 'user',
            route: 'Clients'
        },
        {
            id: 4,
            title: t('suppliers'),
            icon: 'providers',
            route: 'Providers'
        },
        {
            id: 7,
            title: t('interest'),
            icon: 'star',
            route: 'Interests'
        },
        {
            id: 5,
            title: t('myShop'),
            icon: 'shop',
            route: 'MyShop'
        },
        {
            id: 6,
            title: t('settings'),
            icon: 'settings',
            route: 'Settings'
        },

        {
            id: 8,
            title: t('exit'),
            icon: 'exit'
        },
    ];

    const [ current, setCurrent ] = useState(0);
    const dispatch = useDispatch();
    const token = useSelector(state => state.auth.token);

    const [ menu, setMenu ] = useState(DATA);
    useEffect(()=>{
        // itemsFetch(apiService,dispatch,url,type,token);
        setMenu([]);
    },[currentLangCode])
    useEffect(()=>{
        setMenu([]);
    },[token])

    useEffect(()=>{
        if(menu.length === 0) {
            let timer1 = setTimeout(() => setMenu(token===null?DATA.slice(0,2):DATA), 100)

            // this will clear Timeout when component unmount like in willComponentUnmount
            return () => {
                clearTimeout(timer1)
            }
        }
    },[menu])

    return menu.length>0?(
        <SafeAreaView style={{
            backgroundColor: Colors.tintColor,
            flexDirection: "row",
        }}>
            <ScrollView
                horizontal={true}
                contentContainerStyle={{flexGrow: 1, justifyContent: 'space-around'}}>
                {
                    menu.map(item=><Tab key={item.id} current={current} onPress={()=>{
                        setCurrent(item.id);
                        if(item.route){
                            navigation.navigate(item.route);
                        }else{
                            //persistor.purge();
                            dispatch(logout());
                            setCurrent(0);
                            navigation.navigate('Home');
                        }
                    }} {...item}/>)
                }
            </ScrollView>
        </SafeAreaView>
    ):null
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.lang,
    };
};
TabBar = connect(mapStateToProps)(TabBar)

export default withTranslation('main')(TabBar)
