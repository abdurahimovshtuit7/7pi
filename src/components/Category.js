import React from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import Colors from "../constants/Colors";

const Category = ({ item, navigation, next, action, params }) => {

  const { id, image, title, description } = item;

  return <Touch onPress={()=>navigation.navigate('Categories',{ id, next, action, params })}>
    <Image source={{uri:image}} resizeMode="cover">
      <Title>{title}</Title>
      <Desc>{description}</Desc>
    </Image>
  </Touch>
};

const Touch = styled.TouchableOpacity`
    margin: 10px 10px 0px 10px;
    height: 128px;
    overflow: hidden;
`;
const Image = styled.ImageBackground`
    background-color: #FFF;;
    padding: 20px;
    width: 100%; 
    height: 100%;
`;
const Title = styled.Text`
    font-family: LatoBold;
    color: ${Colors.tintColor};
    margin-bottom: 8px;
    font-size: 24px;
    line-height: 29px;
`;
const Desc = styled.Text`
    width: 85%;
    font-family: Lato;
    line-height: 20px;
    font-size: 14px;
`;

export default withNavigation(Category);
