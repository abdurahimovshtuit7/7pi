import React, { useState } from 'react';
import styled from "styled-components/native";
import {Picker,Button,View,Text,StyleSheet,TouchableOpacity} from 'react-native'
import Icon from "../constants/Icons";
import Colors from "../constants/Colors";
import { useDispatch } from "react-redux";
import {connect} from "react-redux";
import { searchUpdate } from "../actions";
import { withNavigation } from "react-navigation";
import UK from '../assets/images/flags/uk'
import RU from '../assets/images/flags/russian'
import UZ from '../assets/images/flags/uzbek'
import Modal from 'react-native-modal';
import {SET_LANGUAGE} from "../constants/actionTypes";
import {withTranslation} from 'react-i18next';



// import TouchableOpacity from "react-native-web/src/exports/TouchableOpacity";

let SearchBar = ({t,i18n,currentLangCode,navigation}) => {

    const [ focus, setFocus ] = useState(false);
    const [ value, setValue ] = useState("");
    const [hand ,setHand] = useState("right")
    const [showModal, setShowModal] = useState(false);
    // const {t} = props

    const toggleModal = () => {
      setShowModal(!showModal)
    }
    const dispatch = useDispatch();
    const onLangClicked = (lang) => {
        // let {i18n, dispatch} = props;
        i18n.changeLanguage(lang, () => {
            dispatch({type: SET_LANGUAGE, lang})

            // console.log("LANGuage:",props.currentLangCode)
        })
    }
    const setSearch =(val) => {
        setValue(val)
        dispatch(searchUpdate(val))
    }
    return <Box>
        {/*<Button title="Show modal" onPress={toggleModal} />*/}
        <TouchableOpacity style={styles.touchable}  onPress={toggleModal}>
            {
                currentLangCode === 'uz'?(<UZ size={27}/>):(
                    currentLangCode === 'ru'?(<RU size={27}/>):(
                       currentLangCode === 'en'?(<UK size={27}/>):(
                            currentLangCode === 'oz'?(<UZ size={27}/>):null
                        )
                    )
                )
            }
            {/*<UK size={27}/>*/}
        </TouchableOpacity>

        {/*<View style={{justifyContent:'center',alignItems: "center"}}>*/}
        <Modal isVisible={showModal}
               onBackdropPress = {toggleModal}
               testID={'modal'}
               animationIn="slideInLeft"
               animationOut="slideOutRight"

        >
            <View style={styles.modal}>
               <TouchableOpacity style={styles.button}
                                 onPress={() => {
                                     onLangClicked('en')
                                     toggleModal()
                                 }}
               >
                   <UK size={30}/>
                   <Text style={styles.text}>
                       English
                   </Text>
               </TouchableOpacity>
                <TouchableOpacity style={styles.button}
                                  onPress={() => {
                                      onLangClicked('ru')
                                      toggleModal()
                                  }}
                >
                    <RU size={30}/>
                    <Text style={styles.text}>
                        Русский
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}
                                  onPress={() => {
                                      onLangClicked('oz')
                                      toggleModal()
                                  }}
                >
                    <UZ size={30}/>
                    <Text style={styles.text}>
                        O'zbek
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}
                                  onPress={() => {
                                      onLangClicked('uz')
                                      toggleModal()
                                  }}
                >
                    <UZ size={30}/>
                    <Text style={styles.text}>
                        Ўзбек (Кирилл)
                    </Text>
                </TouchableOpacity>
                {/*<Button title="Hide modal" onPress={toggleModal} />*/}
            </View>
        </Modal>

        <Btn onPress={()=>value.length>0?navigation.navigate('Search'):null}><Icon name="search" fill="#ffffff" width={18} height={18}/></Btn>
        <Input
            numberOfLines={1}
            returnKeyType="search"
            focus={focus || value.length>0}
            onFocus={()=>setFocus(true)}
            onBlur={()=>setFocus(false)}
            onChangeText={val=>{
             setSearch(val)
            }}
            onSubmitEditing={()=>value.length>0?navigation.navigate('Search'):null}
            placeholder={t('search')} //"поиск компаний товаров"
            placeholderTextColor="#ffffff"
        />
    </Box>
};

const Box = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    flex: 1;
    height: 40px;
    
`;
const Btn = styled.TouchableOpacity`
    padding: 2px 6px;
`;
const Input = styled.TextInput`
    background-color: ${({focus}) => focus ? Colors.white : 'transparent'};
    font-family: Lato;
    flex: 1;
    font-size: 14px;
    border-radius: 16px;
    padding: 2px 12px;
    margin-left: 10px;
    box-shadow: ${({focus}) => focus ? '0 2px 6px rgba(0,148,255,.75)' : 'none'};
    color: #000;

`;
const styles = StyleSheet.create({
    modal: {
        backgroundColor: '#fff',
        paddingVertical: 30,
        // height: "50%",
        // justifyContent:'center',
        borderRadius:10,
        marginHorizontal:15
    },
    text: {
        color: '#3f2949',
        width:'85%',
        fontSize:18,
        // borderWidth: 1
    },
    touchable:{
        // marginRight:10,
        // borderWidth:1,
        paddingRight:10,
        paddingLeft:10,
        paddingVertical:9
    },
    button:{
        paddingHorizontal:20,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems: 'center',
        // borderWidth:1,
        paddingVertical:15,
        marginVertical:3,

    }
    })
const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.lang,
    };
};
SearchBar = connect(mapStateToProps)(SearchBar)
export default withTranslation('main')(withNavigation(SearchBar));
