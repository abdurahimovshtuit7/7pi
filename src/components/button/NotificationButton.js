import React from 'react';
import Icon from "../../constants/Icons";
import styled from "styled-components/native";
import { withNavigation } from "react-navigation"
import { useSelector } from 'react-redux'
import Colors from "../../constants/Colors";

const NotificationButton = ({ navigation }) => {

    const ntfs = useSelector(state => state.notifications);
    let count = 0;
    if(ntfs.items.ads) count+=ntfs.items.ads.length;
    if(ntfs.items.invites) count+=ntfs.items.invites.length;
    if(ntfs.items.comments) count+=ntfs.items.comments.length;

    return <Touch onPress={()=>navigation.navigate('Notification')}>
        <Box><Count>{count}</Count></Box>
        <Icon name="message" width={24} height={24} fill={Colors.white} />
    </Touch>
};

const Touch = styled.TouchableOpacity`
    padding: 4px 16px;
    position: relative;
`;
const Box = styled.View`
    position: absolute;
    top: 0px;
    right: 6px;
    background-color: #e80b0b;
    padding: 1px 5px;
    border-radius: 12px;
    z-index: 1;
`;
const Count = styled.Text`
    font-family: LatoBlack;
    color: #fff;
    width: 100%;
    height: 15px;
    font-size: 11px;
`;

export default withNavigation(NotificationButton);
