import React from 'react';
import Icon from "../../constants/Icons";
import styled from "styled-components/native";
import Colors from '../../constants/Colors';
import {ActivityIndicator} from 'react-native'
import { Platform } from 'react-native';

const Button = ({ type='inline',text,icon,iconRight,progress,onPress=()=>{},flex=false, disabled=false }) => {
  return <Touch onPress={disabled?()=>{}:onPress} type={type} flex={flex} disabled={disabled}>
    { icon && <Icon name={icon} fill={type==='white' || type==='link'?Colors.blue:'#fff'} width={18} height={18} style={{marginRight: 12}} /> }
    <Text type={type}>{progress?<ActivityIndicator animating size={25} color={"#fff"}/>:text}</Text>
    { iconRight && <Icon name={iconRight} fill={type==='white' || type==='link'?Colors.blue:'#fff'} width={12} height={12} style={{ marginTop: 3, marginRight: 3 }} /> }
  </Touch>
};

const handleBgType = type => {
  switch (type) {
    case 'primary': return Colors.tintColor;
    case 'green': return Colors.green;
    case 'white': return Colors.white;
    default: return 'transparent';
  }
};
const handleColorType = type => {
  switch (type) {
    case 'link': return Colors.blue;
    case 'gold': return Colors.gold;
    case 'white': return Colors.blue;
    case 'green-inline': return Colors.green;
    case 'primary-inline': return Colors.tintColor;
    default: return Colors.white;
  }
};
const handleBorderType = type => {
  switch (type) {
    case 'primary': return Colors.tintColor;
    case 'link': return Colors.blue;
    case 'gold': return Colors.gold;
    case 'green': return Colors.green;
    case 'white': return Colors.blue;
    case 'green-inline': return Colors.green;
    case 'primary-inline': return Colors.tintColor;
    default: return Colors.white;
  }
};

const Touch = styled.TouchableOpacity`
    border-color: ${({ type }) => handleBorderType(type)};
    border-width: 1px;
    padding: ${Platform.OS==='ios'?8:6}px 12px;
    margin: 6px;
    background: ${({ type }) => handleBgType(type)};
    border-radius: 6px;
    ${({ flex }) => flex?"flex: 1;":""};
    ${({ type }) => type==='white'?"box-shadow: 0 4px 50px rgba(0,0,0,.5);":""};
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    opacity: ${({ disabled }) => disabled?0.5:1};
`;
const Text = styled.Text`
    font-size: 14px;
    align-self: center;
    color: ${({ type }) => handleColorType(type)};
    font-family: LatoBold;
`;

export default Button;
