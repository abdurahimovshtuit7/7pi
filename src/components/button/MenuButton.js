import React from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation"
import R from "../../constants/R";
import {Image} from "react-native";

const MenuButton = ({ navigation }) => {
  return <Touch onPress={()=>navigation.navigate('Home')}>
      <Image source={R.images.logo} style={{width:24,height:24,marginHorizontal:12}} />
      </Touch>
};

const Touch = styled.TouchableOpacity`
    padding: 8px 5px;
   
`;

export default withNavigation(MenuButton);
