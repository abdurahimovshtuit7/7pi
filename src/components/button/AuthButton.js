import React from 'react';
import Icon from "../../constants/Icons";
import styled from "styled-components/native";
import { withNavigation } from "react-navigation"
import { useSelector } from 'react-redux'
import Colors from "../../constants/Colors";
import NotificationButton from "./NotificationButton";

const AuthButton = ({ navigation }) => {

    const token = useSelector(state => state.auth.token);

    return token===null?<Touch onPress={()=>navigation.navigate('Login')}>
        <Icon name="user" width={24} height={24} fill={Colors.white} />
    </Touch>:<NotificationButton/>
};

const Touch = styled.TouchableOpacity`
    padding: 4px 12px;
`;

export default withNavigation(AuthButton);
