import React from "react";
import { ActivityIndicator, View } from "react-native";
import R from "../../constants/R";

const Spinner = () =>{
    return <View style={R.palettes.container}>
        <ActivityIndicator size={"large"} color={R.colors.tintColor} />
    </View>;
};
export default Spinner;
