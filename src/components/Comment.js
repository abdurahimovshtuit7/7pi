import React from "react";
import styled from "styled-components/native";
import Colors from "../constants/Colors";

const Comment = ({ ava, name, user, text, navigation }) => {

    return <Col>
        <Title>
            <Photo source={{uri:ava}}/>
            <Link onPress={()=>navigation.navigate('CompanyDetails',{id:user})}><Name>{name}</Name></Link>
        </Title>
        <Message>{text}</Message>
    </Col>
};

const Col = styled.View`
    margin: 10px;
    flex: 1;
    padding: 10px;
    background: ${Colors.white};
    box-shadow: 0 2px 15px rgba(0,0,0,.25);
    border-radius: 5px;
`;
const Name = styled.Text`
    font-family: "Lato";
    font-size: 16px;
    color: #007bff;
`;
const Title = styled.View`
    flex-direction: row;
    flex: 1;
    align-items: center;
`;
const Message = styled.Text`
    font-family: "Lato";
    flex: 1;
    font-size: 16px;
    margin: 10px 0px;
    color: ${Colors.text};
`;

const Link = styled.TouchableOpacity`

`;

const Photo = styled.Image`
    width: 40px;
    height: 40px;
    margin-right: 10px;
`;

export default Comment;
