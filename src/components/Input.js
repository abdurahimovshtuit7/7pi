import React, { useState } from 'react';
import styled from "styled-components/native";
import Colors from '../constants/Colors';
import {Platform} from "react-native";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Icon from "../constants/Icons";
import * as ImageManipulator from "expo-image-manipulator";
import {withTranslation} from "react-i18next";

const Input = ({t, label, value,type, icon=null, onEdit=()=>{}, photo=false, secure=false, multiline=false, numberOfLines=1, autoFocus=false, autoCompleteType='off', aspect=[4, 3]}) => {

  const [ focus, setFocus ] = useState(false);
  const [ image, setImage ] = useState(null);

  const _pickImage = async () => {

    if (Platform.OS === 'ios') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      // base64: true,
      quality: 1
    });
     console.log("RESULT:",result)
    if (!result.cancelled) {
      let width = 1280;
      let height = 1280;
      let aspect = 1;

      if(result.width > result.height) {
        aspect = result.width / 1280 ;
        height = result.height / aspect;
      }else{
        aspect = result.height / 1280 ;
        width = result.width / aspect;
      }

      ImageManipulator.manipulateAsync(result.uri, [{resize:{width,height}}], {compress:0.5, format:ImageManipulator.SaveFormat.JPEG}).then(res=>{
        setImage(res.uri);
        onEdit(res.uri);
      });
    }
  };

  return <Box>
    <Text>{label}</Text>
    <Line/>
    <View>
      {icon && <Icon name={icon} width={24} height={24} fill={Colors.tintColor} style={{marginRight: 8}} />}
      {!photo?(type==="phone"?<InputField
          defaultValue={value}
          autoCompleteType={autoCompleteType}
          multiline={multiline}
          numberOfLines={numberOfLines}
          onChangeText={val=>onEdit(val)}
          autoFocus={autoFocus}
          focus={focus}
          placeholder={t("placeHolder")}
          secureTextEntry={secure}
          onFocus={()=>setFocus(true)}
          onBlur={()=>setFocus(false)}
      />:<TextInput
          defaultValue={value}
          autoCompleteType={autoCompleteType}
          multiline={multiline}
          numberOfLines={numberOfLines}
          onChangeText={val=>onEdit(val)}
          autoFocus={autoFocus}
          focus={focus}
          secureTextEntry={secure}
          onFocus={()=>setFocus(true)}
          onBlur={()=>setFocus(false)}
      />):<PhotoInput onPress={!image?_pickImage:()=>setImage(null)}>
        <PhotoLabel>{!image? t("attach"):t("delete")}</PhotoLabel>
      </PhotoInput>}
    </View>
  </Box>
};

const Box = styled.View`
    margin-top: 12px;
    margin-bottom: 12px;
    flex: 1;
`;

const View = styled.View`
    flex-direction: row;
    align-items: center;
`;
const TextInput = styled.TextInput`
    border-bottom-width: 1.5px;
    border-bottom-color: ${props => props.focus ? Colors.blue : "#e8e8e8"};
    font-size: 16px;
    padding: 4px 0px;
    font-family: LatoMedium;
    flex: 1;
    margin-bottom: 0px;
    margin-top: 0px;
`;
const InputField = styled.TextInput`
    border-bottom-width: 1.5px;
    border-bottom-color: ${props => props.focus ? Colors.blue : "#e8e8e8"};
    font-size: 16px;
    padding: 4px 0px;
    font-family: LatoMedium;
    flex: 1;
    margin-bottom: 0px;
    margin-top: 0px;
`;

const PhotoLabel = styled.Text`
    font-family: Lato;
    font-size: 16px;
`;
const PhotoInput = styled.TouchableOpacity`
    border-bottom-width: 1.5px;
    border-bottom-color: ${props => props.focus ? Colors.blue : "#e8e8e8"};
    font-size: 16px;
    padding: 8px 0px;
    flex: 1;
`;
const Text = styled.Text`
    font-size: 16px;
    margin-bottom: 1px;
    color: rgba(27,28,41,.5);
    font-family: Lato;
`;
const Line = styled.Text`
    height: 1px;
    width: 32px;
    background: #003e7a;
`;

export default withTranslation('main')(Input);
