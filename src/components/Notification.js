import React from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import Colors from "../constants/Colors";
import Icon from "../constants/Icons";
import {useDispatch, useSelector} from "react-redux";
import {feedUpdate} from "../actions";

const Notification = ({ item, navigation, setRefresh, api }) => {

    const dispatch = useDispatch();

    const token = useSelector(state => state.auth.token);

    const getText = () => {
        switch (item.type) {
            case "comment":
                return `Пользователь ${item.username} оставил комментарий`;
            case "invite":
                return `Пользователь ${item.userName} приглашает Вас в обсуждение`;
            default:
                return `Новое объявление в разделе ${item.catName}`;
        }
    };

    const add = () => {
        switch (item.type) {
            case "ad": api.addToFeed(item.ad,token).then(()=>{
                dispatch(feedUpdate(new Date()));
                navigation.navigate('Feed');
            }); break;
            default: break;
        }
    };

    const remove = () => {
        api.removeNotification(item.ad,item.type,token).then(()=>{
            setRefresh(new Date());
        });
    };

  return <Touch onPress={add}>
      <Title>{getText()}</Title>
      <Close onPress={remove}><Icon name="close" width={20} height={20}/></Close>
  </Touch>
};

const Touch = styled.TouchableOpacity`
    padding: 20px;
    background: #ffffff;
    box-shadow: 0 4px 50px rgba(0, 0, 0, 0.1);
    flex-direction: row;
`;
const Title = styled.Text`
    font-family: LatoMedium;
    color: ${Colors.blue};
    margin-bottom: 8px;
    flex: 1;
    font-size: 18px;
`;
const Close = styled.TouchableOpacity`
    padding: 6px;
`;
export default withNavigation(Notification);
