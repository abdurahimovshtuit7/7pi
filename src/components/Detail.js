import React,{useState} from 'react';
import {Text, Linking, Clipboard, TouchableOpacity, StyleSheet} from 'react-native'
import styled from "styled-components/native";
import Toast from '../components/Toast/index'
import Modal from "react-native-modal";
import UK from "../assets/images/flags/uk";
import RU from "../assets/images/flags/russian";
import UZ from "../assets/images/flags/uzbek";
import {Feather} from '@expo/vector-icons';



const Detail = ({item, value,type, url,modal}) => {
    console.log(`https://7pi.uz/c/${value}`)
    const [showModal, setShowModal] = useState(false);
    // const {t} = props

    const toggleModal = () => {
        setShowModal(!showModal)
    }

    return (
        <TouchableOpacity activeOpacity={1}>
        <Modal isVisible={showModal}
               onBackdropPress = {toggleModal}
               testID={'modal'}
               animationIn="slideInLeft"
               animationOut="slideOutRight"

        >
            <TouchableOpacity activeOpacity={1} style={styles.modal}>
                <TouchableOpacity activeOpacity={1} style={styles.xButton}>
                <TouchableOpacity style={styles.cancel} onPress={()=>{
                    toggleModal()
                }}>
                    <Feather name="x" size={24} color="white" />
                </TouchableOpacity>
                </TouchableOpacity>

               <Text style={styles.text}>
                   {modal}
               </Text>
                {/*<Button title="Hide modal" onPress={toggleModal} />*/}
            </TouchableOpacity>
        </Modal>
            { type===1?(
                <View>
                    <Title>{item}</Title>
                    {
                        url === "1" ? (<Desc style={{color: 'blue'}} onPress={() => {
                            Linking.openURL(value)
                        }}>{value}</Desc>) : (
                            url === "2" ? (
                                <Desc style={{color: 'blue'}} selectable onPress={() => {
                                    toggleModal()
                                    Clipboard.setString(`https://7pi.uz/c/${value}`)
                                }}>{`https://7pi.uz/c/${value}`}</Desc>
                            ) : (
                                <Desc selectable>{value}</Desc>
                            )

                        )
                    }
                </View>
            ):(
                type===2?(
                    <BoxView>
                    <Title>{item}</Title>
                        <Row onPress={() => {
                            toggleModal()
                            Clipboard.setString(`https://7pi.uz/c/${value}`)
                        }}>
                            <Feather name={"copy"}  size={24} color="#aa1" style={styles.icon}/>
                    {
                        url === "1" ? (<Desc style={{color: 'blue'}} onPress={() => {
                            Linking.openURL(value)
                        }}>{value}</Desc>) : (
                            url === "2" ? (
                                <Desc style={{color: 'blue',width:'50%'}}>{`https://7pi.uz/c/${value}`}</Desc>
                            ) : (
                                <Desc selectable>{value}</Desc>
                            )

                        )
                    }
                        </Row>
                </BoxView>
                ):(
                    <Box>
                        <Title>{item}</Title>
                        {
                            url === "1" ? (<Desc style={{color: 'blue'}} onPress={() => {
                                Linking.openURL(value)
                            }}>{value}</Desc>) : (
                                url === "2" ? (
                                    <Desc  style={{color: 'blue'}} selectable onPress={() => {
                                        toggleModal()
                                        Clipboard.setString(`https://7pi.uz/c/${value}`)
                                    }}>{`https://7pi.uz/c/${value}`}</Desc>
                                ) : (
                                    <Desc selectable>{value}</Desc>
                                )

                            )
                        }
                    </Box>
                )

            )}
        </TouchableOpacity>
    )






};

const Box = styled.View`
    flex-direction: row;
    border-bottom-color: #e8e8e8;
    border-bottom-width: 1px;
    padding-bottom: 16px;
    margin-bottom: 16px;
    align-items: center;
`;
const BoxView = styled.View`
  
    border-bottom-color: #e8e8e8;
    border-bottom-width: 1px;
    padding-bottom: 16px;
    margin-bottom: 16px;
  
`
const View = styled.View`
    border-bottom-color: #e8e8e8;
    border-bottom-width: 1px;
    padding-bottom: 16px;
    margin-bottom: 16px;
    
`;
const Row = styled.TouchableOpacity`
    flex-direction: row;
    border-bottom-color: #e8e8e8;
    padding-top:15px;
    align-items: center;
`
const Title = styled.Text`
    flex: 2;
    font-family: Lato;
    color: #aaa;
    font-size: 14px;
`;
const Desc = styled.Text`
    flex: 3;
    font-family: LatoMedium;
    font-size: 16px;
`;
const styles = StyleSheet.create({
    modal: {
        backgroundColor: "#00305f",
        paddingVertical: 15,
        // height: "50%",
        // justifyContent:'center',
        borderRadius:10,
        marginHorizontal:15,

    },
    text: {
        color: '#fff',
        width:'100%',
        fontSize:18,
        textAlign:'center',
        paddingHorizontal: 15,
        paddingVertical:25,
        // borderWidth: 1
    },
    touchable:{
        // marginRight:10,
        // borderWidth:1,
        paddingRight:10,
        paddingLeft:10,
        paddingVertical:9
    },
    button:{
        paddingHorizontal:20,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems: 'center',
        // borderWidth:1,
        paddingVertical:15,
        marginVertical:3,

    },
    cancel:{
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth:1,
        width: 30,
        height:30
    },
    xButton:{
        alignItems:'flex-end',
        paddingHorizontal:20

    },
    icon:{
        width:'40%',
        paddingLeft: 5,
    }
})

export default Detail;
