import * as React from "react"
import { Animated, TouchableOpacity } from "react-native"
import { withNavigation } from "react-navigation"
import Icon from "../constants/Icons";
import Colors from "../constants/Colors";

const Tab = ({ title, icon, onPress, id, current }) => {

    return (
        <TouchableOpacity onPress={onPress}>
            <Animated.View
                style={{
                    paddingHorizontal: 16,
                    paddingVertical: 4,
                    alignItems: "center"
                }}
            >
                <Icon name={icon} width={26} height={26} fill={current===id?Colors.gold:Colors.white}/>
                <Animated.Text style={{marginTop:2,color:current===id?Colors.gold:Colors.white}}>{title}</Animated.Text>
            </Animated.View>
        </TouchableOpacity>
    )
};

export default withNavigation(Tab)
