import React, {useEffect, useState} from "react";
import styled from 'styled-components/native';
import Colors from "../constants/Colors";
import SectionedMultiSelect from 'expo-sectioned-multi-select';

// import SectionedMultiSelect from "react-native-sectioned-multi-select";
import {withTranslation} from "react-i18next";
import {connect} from "react-redux";

let CheckBoxSectioned = ({label, t, values=[], onChange, data=[] }) => {

    const add = selected => {
        if (selected.length > 5) {
            alert(t("max_five"));
        } else {
            onChange(selected)
        }
    }

console.log("data:",t)
    return <>
        <LabelBox>
            <Label>{label}</Label>
        </LabelBox>
        <SectionedMultiSelect
            styles={{
                selectToggle:{
                    marginHorizontal: 0,
                    marginBottom: 12,
                    paddingBottom: 12,
                },
                chipsWrapper: {
                    margin: 0,
                    marginBottom: 12,
                    paddingBottom: 12,
                }
            }}
            items={data}
            uniqueKey='id'
            displayKey='title'
            subKey='children'
            selectText={t("select_search")}
            selectedText={t("selected")}
            searchPlaceholderText={t("search_text")}
            confirmText={t("save")}
            removeAllText={t("delete")}
            readOnlyHeadings={true}
            onSelectedItemsChange={add}
            selectedItems={values}
        />
    </>
};
const LabelBox = styled.View`
    flex-direction: row;
    margin-bottom: 16px;
`;
const Label = styled.Text`
    font-family: "Lato";
    color: ${Colors.muted};
    font-size: 18px;
`;
const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode:state.lang
    };
};

export default withTranslation('main')(CheckBoxSectioned);
