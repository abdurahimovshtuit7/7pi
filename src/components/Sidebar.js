import React from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import Icon from "../constants/Icons";

const Sidebar = () => {
    return <Box>
        <Header>
            <Title>Vitra. Furniture group</Title>
            <Icon name="close" width={16} height={16} fill={Colors.white}/>
        </Header>
        <Cover source={{uri:`https://7pi.uz/images/covers/cover45.jpg?t=1550255377`}}>
            <Logo source={{uri:`https://7pi.uz/images/users/user45.jpg?t=1550255377`}} resizeMode="contain" />
        </Cover>
        <Content>
            <ContentBox>
                <Link><Icon name="info" width={20} height={20} /><LinkText>Информация</LinkText></Link>
                <Link><Icon name="shop" width={20} height={20} /><LinkText>Мой магазин</LinkText></Link>
                <Link><Icon name="settings" width={20} height={20} /><LinkText>Настройки</LinkText></Link>
                <Link><Icon name="exit" width={20} height={20} /><LinkText>Выйти</LinkText></Link>
            </ContentBox>
        </Content>
    </Box>
};

const Box = styled.SafeAreaView`
    background: ${Colors.blue};
    flex: 1;
`;
const Cover = styled.ImageBackground`
    width: 100%;
    height: 160px;
    padding: 16px;
    justify-content: center;
`;
const Logo = styled.Image`
    width: 60px;
    height: 60px;
    border-radius: 6px;
    background-color: #ffffff;
`;
const Header = styled.View`
    width: 100%;
    padding: 12px;
    flex-direction: row;
`;
const Content = styled.View`
    background: ${Colors.bg};
    flex: 1;
`;
const ContentBox = styled.View`
    background: ${Colors.white};
    border-radius: 6px;
    flex: 1;
    margin: 16px;
    padding: 16px;
`;
const Link = styled.TouchableOpacity`
    flex-direction: row;
    padding-bottom: 16px;
    padding-top: 16px;
    border-bottom-width: 1px;
    border-bottom-color: #E8E8E8;
    align-items: center;
`;
const LinkText = styled.Text`
    margin-left: 8px;
    color: ${Colors.blue};
    font-family: LatoMedium;
    font-size: 20px;
`;
const Title = styled.Text`
    flex: 1;
    font-family: LatoMedium;
    color: #fff;
    font-size: 14px;
    text-transform: uppercase;
    text-align: center;
`;

export default Sidebar;
