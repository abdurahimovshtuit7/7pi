import React from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import Colors from "../constants/Colors";

const SubCategory = ({ item, navigation, next, action, params }) => {

  const { id, title } = item;

  return <Touch onPress={()=>navigation.navigate( next, { id, action, params } )}>
      <Title>{title}</Title>
  </Touch>
};

const Touch = styled.TouchableOpacity`
    margin: 16px 16px 0px 16px;
`;
const Title = styled.Text`
    font-family: LatoMedium;
    color: ${Colors.blue};
    margin-bottom: 8px;
    font-size: 18px;
`;

export default withNavigation(SubCategory);
