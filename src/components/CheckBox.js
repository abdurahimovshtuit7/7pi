import React, { useState } from 'react';
import Icon from "../constants/Icons";
import styled from "styled-components/native";
import Colors from '../constants/Colors';

const CheckBox = ({ text, onPress=()=>{}}) => {

  const [ checked, setChecked ] = useState(false);

  return <Touch onPress={()=>{setChecked(!checked);onPress(!checked);}}>
    <Icon name={checked?"check":"uncheck"} fill={Colors.blue} width={18} height={18} style={{marginRight: 12}} />
    <Text>{text}</Text>
  </Touch>
};

const Touch = styled.TouchableOpacity`
    padding: 12px 0px;
    margin: 6px 0px;
    border-radius: 6px;
    display: flex;
    flex:0.5;
    flex-direction: row;
`;
const Text = styled.Text`
    font-size: 14px;
    flex: 1;
    color: ${Colors.blue};
    font-family: LatoBold;
`;

export default CheckBox;
