import React, { useState, useEffect } from 'react';
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import { LinearGradient } from 'expo-linear-gradient';
import R from "../constants/R";
import Colors from "../constants/Colors";
import Button from "./button/Button";
import Icon from "../constants/Icons";
import {useDispatch, useSelector} from "react-redux";
import { showMessage } from "react-native-flash-message";
import {addClient, addProvider, companySelect} from "../actions";
import {withTranslation} from "react-i18next";

const Company = ({ item, navigation, action='add', api, setRefresh, params,t }) => {

    const { nik, catNames, address, photo, cover, user } = item;

    const [ actionButton, setActionButton ] = useState(null);

    const selectedCompany = useSelector(state => state.selectedCompany);

    const token = useSelector(state => state.auth.token);
    const profile = useSelector(state => state.user);
    const dispatch = useDispatch();

    const remove = () => {
        api.removePartner(user,token).then(res=>setRefresh(new Date()));
    };

    const getButtons = () => {
        if(!profile || !profile.providers || !profile.clients) return null;
      if(Object.values(profile.providers).indexOf(user)>-1) {
          return <Button text={t("supplier")} icon="check" flex={true} type="white" disabled={true}/>
      } else if(Object.values(profile.clients).indexOf(user)>-1) {
          return <Button text={t("client")} icon="check" flex={true} type="white" disabled={true}/>
      } else {
          return <>
              <Button text={t("supplier")} icon="plus" flex={true} type="white" onPress={()=>addProvider(api,dispatch,user,token)}/>
              <Button text={t("client")} icon="plus" flex={true} type="white" onPress={()=>addClient(api,dispatch,user,token)}/>
              </>
      }
    };

    const send = () => {
        api.sendPost(params.id,params.ad,user,token).then(()=>{
            setActionButton(null);
            showMessage({
                message: t("success_send"),
                backgroundColor: Colors.tintColor,
                type: "info",
            })
        });
    };

    useEffect(()=>{
        switch (action) {
            case "delete": setActionButton(<Delete onPress={remove}><Icon name="close" width={20} height={20}/></Delete>); break;
            case "send": setActionButton(<Send onPress={send}><Icon name="send" width={20} height={20} fill={Colors.green}/></Send>); break;
            default: setActionButton(null);
        }
    },[]);

    return <Container onPress={()=>selectedCompany===item.user?navigation.navigate('CompanyDetails',{id:user}):dispatch(companySelect(item.user))}>
        <Image source={{uri:cover}}>
        </Image>
        {selectedCompany===item.user?<Overlay
            colors={['rgba(255,255,255,0.6)', 'rgba(255,255,255,1)']}
            start={[1,0]}
            end={[0,0]}
            locations={[0,.5]}
            style={{
                position: 'absolute',
                left: 0,
                bottom: 0,
                top: 0,
                height: 220,
                width: R.strings.width - 32
            }}>
            <TitleBlack>{nik}<Icon name="caret" width={16} height={16}/></TitleBlack>
            {catNames.map((item,index)=><Category key={'cat'+index}>{item}</Category>)}
            { action === 'add' && <Col>
                {getButtons()}
            </Col>}
        </Overlay>:<Overlay
                locations={[0, 1]}
                colors={['rgba(0,0,0,0)', 'rgba(0,0,0,0.5)']}>
                {actionButton}
                <Logo source={{uri:photo}} resizeMode="contain" />
                <Box>
                    <Title>{nik}<Icon name="caret" width={16} height={16} fill={Colors.white}/></Title>
                    <Address>{address}</Address>
                </Box>
            </Overlay>
            }
    </Container>
};

const Container = styled.TouchableOpacity`
    margin: 16px 16px 0px 16px;
    z-index: 0;
`;
const Image = styled.ImageBackground`
    background-color: #FFF;
    position: relative;
    padding: 20px;
    width: 100%;
    height: 220px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;
const Title = styled.Text`
    color: #ffffff;
    font-family: LatoBold;
    margin-bottom: 2px;
    font-size: 20px;
`;
const TitleBlack = styled.Text`
    color: ${Colors.blue};
    font-family: LatoBold;
    margin-bottom: 12px;
    font-size: 20px;
`;
const Address = styled.Text`
    color: #ffffff;
    font-family: Lato;
    font-size: 14px;
`;
const Category = styled.Text`
    color: #444444;
    font-family: Lato;
    font-size: 16px;
    margin-bottom: 2px;
`;
const Overlay = styled(LinearGradient)`
    position:absolute;
    width: ${R.strings.width-32};
    height: 220px;
    z-index: 1;
    padding: 8px;
`;
const Box = styled.View`
    position: absolute;
    bottom: 8px;
    left: 8px;
`;
const Col = styled.View`
    position: absolute;
    bottom: 8px;
    left: 4px;
    right: 4px;
    flex-direction: row;
`;
const Logo = styled.Image`
    width: 80px;
    height: 80px;
    border-radius: 6px;
    background-color:#FFFFFF;
`;
const Delete = styled.TouchableOpacity`
    padding: 10px;
    position: absolute;
    top: 10px;
    right: 10px;
    background-color: #ffffff;
    border-radius: 32px;
    z-index: 2;
`;
const Send = styled.TouchableOpacity`
    padding: 10px;
    position: absolute;
    align-items: center;
    justify-content: center;
    top: 10px;
    right: 10px;
    background-color: #ffffff;
    border-radius: 32px;
    z-index: 2;
`;
export default withTranslation('main')(withNavigation(Company));
