import React, { useEffect, useState } from 'react';
import withApiService from "../../components/hoc";
import Spinner from '../../components/spinner/Spinner'
import Comment from "../../components/Comment";
import {useDispatch} from "react-redux";
import {commentsUpdate} from "../../actions";

const CommentListContainer = ({ apiService, ad, user, navigation, token, updateComments }) => {

    const [ list, setList ] = useState([]);
    const [ loading, setLoading ] = useState(true);
    const id = user ? "po" + ad : "ad" + ad;
    const dispatch = useDispatch();

    useEffect(()=>{
        getComments();
    },[]);

    useEffect(()=>{
        console.log('ubnububuin',updateComments);
        if(updateComments===id) {
            getComments();
            dispatch(commentsUpdate(null));
        }
    });

    const getComments = () => {
        apiService.getResources('get?id='+id,token).then(res=>{
            res.commments.map(i=>console.log(i.text));
            setList(res.commments);
            setLoading(false);
        })
    };

    const _renderItem = (item) => <Comment/>;

    if(loading) return <Spinner/>;

    return list.map(item=>item.comments.map(i=>{
            return <Comment key={`com`+i.id} user={i.user} ava={i.userphoto} name={i.username} navigation={navigation} text={i.text}/>
        }))
};

export default withApiService()(CommentListContainer);
