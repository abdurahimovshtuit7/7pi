import React, { useEffect, useState ,useMemo} from 'react';
import {connect, useDispatch, useSelector} from "react-redux";
import withApiService from "../../components/hoc";
import {companySelect, itemsFetch,Page, productSelect} from "../../actions"
import Spinner from '../../components/spinner/Spinner'
import ErrorIndicator from "../../components/error-indicator/ErrorIndicator";
import { FlatList } from "react-native"
import Category from "../../components/Category";
import SubCategory from "../../components/SubCategory";
import Product from "../../components/Product";
import Company from "../../components/Company";
import Post from "../../components/Post";
import Notification from "../../components/Notification";

import {get} from 'lodash'
// import * as SecureStore from "expo-secure-store";
// import i18next from 'i18next';

// const lang = get(i18next,'language[0]','uz')

let ItemListContainer = (props) => {

    const { apiService, type, url, next, action, refresh, setRefresh, params, isFocused,language } = props
    const state = useSelector(state => state);
    // const lang = useSelector(state => state.lang);
    const page = useSelector(state => state.page);
    const token = state.auth.token;
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(companySelect(null));
        dispatch(productSelect(null));
    },[url, refresh, type, isFocused]);

    useEffect(()=>{
        console.log("LANGUAGE:",language)
        // lang !== nextprops.currentLang
        itemsFetch(apiService,dispatch,url,type,token);
    },[refresh]);
    // useMemo(() => {
    //     itemsFetch(apiService,dispatch,url,type,token);
    // },[...props]);
    useEffect(()=>{
        itemsFetch(apiService,dispatch,url,type,token);
    },[props.language])
    useEffect(()=>{
        console.log(token);
        if(isFocused !== undefined) itemsFetch(apiService,dispatch,url,type,token);
    },[isFocused,props.language]);

    let _renderItem = (item) => {
        // console.log("Render_Item:",{...item})
        // console.log("apiService:",apiService)
        switch (type) {
            case "categories": return <Category next={next} action={action} params={params} {...item}/>;
            case "subcategories": return <SubCategory action={action} params={params} next={next} {...item}/>;
            case "companies": return <Company {...item} params={params} api={apiService} action={action} setRefresh={setRefresh}/>;
            case "products": return <Product api={apiService} {...item}/>;
            case "shop": return <Product api={apiService} {...item} setRefresh={setRefresh} pop={true} action={action}/>;
            case "clients": return <Company {...item} params={params} api={apiService} action={action} setRefresh={setRefresh}/>;
            case "providers": return <Company {...item} params={params} api={apiService} action={action} setRefresh={setRefresh}/>;
            case "feed": return <Post api={apiService} setRefresh={setRefresh} {...item}/>;
            case "notifications": return <Notification api={apiService} setRefresh={setRefresh} {...item}/>;
            case "searchCompanies": return <Company api={apiService} {...item}/>;
            case "searchProducts": return <Product api={apiService} {...item}/>;
            default: return null;
        }
    };

    const { items, loading, error } = state[type];

    if(loading) return <Spinner/>;

    if(error) return <ErrorIndicator error={error}/>;

    let data = [];

    switch (type) {
        case 'notifications': data=items.ads; break;
        case 'searchProducts': data=items.ads; break;
        case 'searchCompanies': data=items.profiles; break;
        default: data = items;
    }
    console.log("data:",page)
    return <FlatList
        data={data}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        renderItem={_renderItem}
        onEndReached={() => {
            // if (data) {
            //     let p = 1
            //     p++
            //     dispatch(Page(p))
            //      // let pg = page
            //     // itemsFetch(apiService,dispatch,url,type,token,page);
            //
            //     console.log("PAGE:", page)
            //     // this.getItems(page, false, true)
            // }
        }}
        keyExtractor={(item, index) => index.toString()}
    />
};
const mapStateToProps = (state, ownProps) => {
    return {
        // currentLangCode: state.lang,
    };
};
// ItemListContainer = connect(mapStateToProps)(ItemListContainer)
export default  connect(mapStateToProps)(withApiService()(ItemListContainer));
