import React, { Component } from 'react';
import { connect } from "react-redux";
import withApiService from "../../components/hoc";
import Spinner from '../../components/spinner/Spinner'
import ErrorIndicator from "../../components/error-indicator/ErrorIndicator";
import { companyFetch } from "../../actions"
import CompanyDetails from "../../components/CompanyDetails";

class CompanyContainer extends Component{

    componentDidMount() {
        this.props.companyFetch();
    }

    render() {
        const { item, loading, error } = this.props;

        if(loading) return <Spinner/>;

        if(error) return <ErrorIndicator error={error}/>;

        return <CompanyDetails item={item} api={this.props.apiService}/>

    }
}

const mapStateToProps = ({company: { item, loading, error }}) => {
    return { item, loading, error }
};

const mapDispatchToProps = (dispatch, { apiService, id }) => {

    return{
        companyFetch: companyFetch( apiService, dispatch, id )
    }
};

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(CompanyContainer));
