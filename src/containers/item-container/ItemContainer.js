import React, { Component } from 'react';
import { connect } from "react-redux";
import withApiService from "../../components/hoc";
import Spinner from '../../components/spinner/Spinner'
import ErrorIndicator from "../../components/error-indicator/ErrorIndicator";
import { productFetch } from "../../actions"
import ProductDetails from "../../components/ProductDetails";

class ItemContainer extends Component{

    componentDidMount() {
        this.props.productFetch();
    }

    render() {
        const { item, loading, error } = this.props;

        if(loading) return <Spinner/>;

        if(error) return <ErrorIndicator {...error}/>;

        return <ProductDetails item={item}/>

    }
}

const mapStateToProps = ({product: { item, loading, error }}) => {
    return { item, loading, error }
};

const mapDispatchToProps = (dispatch, { apiService, id }) => {

    return{
        productFetch: productFetch( apiService, dispatch, id )
    }
};

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(ItemContainer));
