import React, { useEffect } from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import ItemListContainer from "../containers/item-list-container/ItemListContainer";
import { useSelector } from "react-redux";

export default function SearchScreen({ navigation }) {

  const search = useSelector(state=>state.search);
  console.log("search:",search)
  return <Box>
    <Title>Товары</Title>
    <ItemListContainer url={'search?q='+search} type={'searchProducts'} />
    <Title>Компании</Title>
    <ItemListContainer url={'search?q='+search} type={'searchCompanies'} />
  </Box>;
}

const Box = styled.ScrollView`
  background: ${Colors.bg};
`;

const Title = styled.Text`
    color: #000;
    text-transform: uppercase;
    font-size: 22px;
    margin-bottom: 20px;
    text-align: center;
    font-family: LatoMedium;
`;
const Text = styled.Text`
    color: #ffffff;
    font-size: 16px;
    text-align: center;
    font-family: Lato;
`;
