import React, { useEffect ,useMemo} from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import ItemListContainer from "../containers/item-list-container/ItemListContainer";
import registerForPushNotificationsAsync from "../services/notification"
import strings from "../constants/Strings";
import {useSelector} from "react-redux";
import Button from "../components/button/Button";
import {withTranslation} from "react-i18next";
import {connect} from "react-redux";
import i18next from 'i18next';
import {get} from "lodash";

function HomeScreen({t, currentLangCode,navigation }) {
  const lang = get(i18next,'languages[0]','')

  const token = useSelector(state => state.auth.token);
  // const {t} = props
  console.log(t('longText'))
  // useMemo(() => {
  //
  // },[...props]);
  useEffect(()=>{
    console.log("I!*NEXT:",get(i18next,`options.resources.${lang}.main.search`,''))
    registerForPushNotificationsAsync(token).then(res=>res.json().then(json=>console.log(json)))
    console.log("blablabla",lang)
    },[]);

  return <Box>
    <Bg source={{uri:"https://7pi.uz/images/slider1.jpg"}}>
      <Title>{t('longText')}</Title>
      {/*Единая Национальная информационная бизнес-платформа Узбекистана*/}
      <Text>{t("description")}</Text>
      {!token && <Button onPress={()=>navigation.navigate('Signup')} type="gold" text={t("become_member")} flex icon="plus" />}
    </Bg>
    <ItemListContainer url={'market'} type={'categories'} next={'products'} language={currentLangCode} />
  </Box>;
}



const Box = styled.ScrollView`
    background: ${Colors.bg};
`;

const Bg = styled.ImageBackground`
    flex: 1;
    width: ${strings.width-20};
    padding: 15px;
    margin: 10px 10px 0px 10px;
`;

const Title = styled.Text`
    color: #ffffff;
    text-transform: uppercase;
    font-size: 22px;
    line-height: 28px;
    margin-top: 11px;
    margin-bottom: 24px;
    text-align: center;
    font-family: LatoMedium;
`;
const Text = styled.Text`
    color: #ffffff;
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    margin-bottom: 10px;
    text-align: center;
    font-family: Lato;
`;
const mapStateToProps = (state, ownProps) => {
  return {
    currentLangCode:state.lang
  };
};


HomeScreen = connect(mapStateToProps)(HomeScreen)
HomeScreen.navigationOptions = {
  header: null,
};
export default withTranslation('main')(HomeScreen)
