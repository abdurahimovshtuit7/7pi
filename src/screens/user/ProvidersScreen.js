import React, { useState } from 'react';
import styled from "styled-components/native/dist/styled-components.native.esm";
import Colors from "../../constants/Colors";
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";
import { withNavigationFocus } from "react-navigation";

function ProvidersScreen({ navigation, isFocused }) {

  const action = navigation.getParam('action');
  const params = navigation.getParam('params');

  const [ refresh, setRefresh ] = useState(new Date());

  return <Box>
    <ItemListContainer url={'providers'} type="providers" action={action?action:"delete"} params={params} refresh={refresh} setRefresh={setRefresh} isFocused={isFocused}/>
  </Box>;
}

ProvidersScreen.navigationOptions = {
  header: null,
};

export default withNavigationFocus(ProvidersScreen);

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
