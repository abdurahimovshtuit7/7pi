import React, { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import {Text} from "react-native";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Input from "../../components/Input";
import Button from "../../components/button/Button";
import withApiService from "../../components/hoc";
import CheckBoxSectioned from "../../components/CheckBoxSectioned";
import {withTranslation} from "react-i18next";

function SettingsScreen({ apiService,t }) {

  const token = useSelector(state => state.auth.token);
  const [cats, setCats] = useState([]);

  const [form, setState] = useState({
    name: "",
    surname: "",
    website: "",
    address: "",
    photo: null,
    cover: null,
    phone: "",
    phone2: "",
    companyname: "",
    cats: [],
    companytext: "",
    license: null,
    type: -1,
    cert: null,
    companylink:""
  });

  const updateField = (e,param) => {
    setState({
      ...form,
      [param]: e
    });
  };

  useEffect(()=>{
    apiService.getResources('edit',token)
        .then(res=>{

          console.log('resssss:',res.profile)
          setState(res.profile);
          // res.profile.cats === ""?setState({cats: []}):null
          setCats(res.cats);

        })
        .catch()
  },[]);

  const save = () => {
    apiService.editProfile(form,token).then(res=>{
      alert(t("success"))
      console.log("resp:",res)
    })
  }
  console.log("CATS:",form.cats)
  return <Box>
    <Title>{t("data_personal")}</Title>
    <Input label={t("first_name")} value={form.name} onEdit={val=>updateField(val,'name')}/>
    <Input label={t("last_name")} value={form.surname} onEdit={val=>updateField(val,'surname')}/>
    <Input label={t("photo_logotip")} value={form.photo} icon="photo" photo={true} onEdit={val=>updateField(val,'photo')}/>
    <Title>{t("company")}</Title>
    <Input label={t("company")} value={form.companyname} onEdit={val=>updateField(val,'companyname')}/>
    {form.type===1 && (
      <CheckBoxSectioned
      values={cats.length>1?form.cats.map((i) =>
            parseInt(i)
       ):[]}
      label={t("activity")}
      placeholder={t("select_search")}
      onChange={val=>updateField(val,'cats')}
      data={cats}
      // t={t}
      />
      ) }
    <Input multiline={true} label={t("company_description")} value={form.companytext} onEdit={val=>updateField(val,'companytext')}/>
    <Input label={t("address")} value={form.address} onEdit={val=>updateField(val,'address')}/>
    <Input label={t("phone")} type={"phone"} value={form.phone} onEdit={val=>updateField(val,'phone')}/>
    <Input label={`${t("phone")} 2`} type={"phone"} value={form.phone2} onEdit={val=>updateField(val,'phone2')}/>
    <Input label={t("link")} value={form.companylink} onEdit={val=>updateField(val,'companylink')}/>
    <Text style={{color:"#595A65"}}>{t("link_valid")}</Text>
    <Input label={t("web_site")} value={form.website} onEdit={val=>updateField(val,'website')}/>
    <Input label={`${t("banner_profile")} (1300x400)`} aspect={[13,4]} value={form.cover} icon="photo" photo={true} onEdit={val=>updateField(val,'cover')}/>
    <Input label={t("guvohnoma")} value={form.license} icon="photo" photo={true} onEdit={val=>updateField(val,'license')}/>
    <Input label={t("certificate")} value={form.cert} icon="photo" photo={true} onEdit={val=>updateField(val,'cert')}/>
    <Button text={t("save")} type="primary" onPress={()=>save()}/>
    <Title></Title>
    <Title></Title>
  </Box>;
}

SettingsScreen.navigationOptions = { header: null };

export default withApiService()(withTranslation('main')(SettingsScreen));

const Box = styled.ScrollView`
  flex: 1;
  padding: 16px;
  background: ${Colors.white};
`;
const Title = styled.Text`
    font-size: 24px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;
