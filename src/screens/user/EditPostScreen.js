import React, { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Input from "../../components/Input";
import Button from "../../components/button/Button";
import withApiService from "../../components/hoc";
import { withNavigation } from "react-navigation";
import {withTranslation} from "react-i18next";

function EditPostScreen({ apiService, navigation,t }) {

  const id = navigation.getParam('id');

  const setRefresh = navigation.getParam('setRefresh');

  const token = useSelector(state => state.auth.token);

  const [text, setText] = useState(null);

  useEffect(()=>{
    apiService.getResources('edit-post?id='+id,token)
        .then(res=>{
          setText(res.text);
        })
        .catch()
  },[]);

  const save = () => {
    apiService.addPost(text,token,null,id).then(res=>{
      setRefresh(new Date());
      navigation.goBack();
    });
  };

  return <Box>
    <Input label={t("tavsif")} value={text} multiline={true} numberOfLines={3} onEdit={val=>setText(val)}/>
    <Input label={t("photo")} icon="photo" photo={true} onEdit={()=>{}}/>
    <Button text={t("save")} type="primary" onPress={save}/>
  </Box>;
}

EditPostScreen.navigationOptions = { header: null };

export default withApiService()(withTranslation('main')(withNavigation(EditPostScreen)));

const Box = styled.ScrollView`
  flex: 1;
  padding: 16px;
  background: ${Colors.white};
`;
const Title = styled.Text`
    font-size: 24px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;
