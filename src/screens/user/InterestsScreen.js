import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import {itemsFetch} from "../../actions";
import withApiService from "../../components/hoc";
import styled from "styled-components/native";
import Spinner from "../../components/spinner/Spinner";
import Icon from "../../constants/Icons";
import Colors from "../../constants/Colors";
import { withNavigationFocus } from "react-navigation";

function InterestsScreen({ apiService, isFocused }) {

    const token = useSelector(state => state.auth.token);
    const items = useSelector(state => state.interests.items);
    const dispatch = useDispatch();

    useEffect(()=>{
        itemsFetch( apiService, dispatch, 'interests', 'interests', token );
    },[isFocused]);

    return <Box>
        {items && items.cats ? items.cats.map(cat=><Category key={'cat'+cat.id} token={token} apiService={apiService} data={items.selectedIds} cat={cat}/>) : <Spinner/>}
    </Box>

}

const Category = ({ cat, data, apiService, token }) => {
    console.log("data:",data)

    const [ open, setOpen] = useState(false);

    return <CategoryBox>
        <Header onPress={()=>setOpen(!open)}>
            <Title>{cat.title}</Title>
            <Icon name={open?"up":"down"} width={20} height={20} fill={Colors.tintColor} />
        </Header>
        { open && <Subs>
            {cat.children && cat.children.map(item=><Sub key={'sub'+item.id} token={token} apiService={apiService}
                                                         selected={data.length?data.includes(item.id):null}
                                                         sub={item}/>)}
        </Subs>}
    </CategoryBox>
};

const Sub = ({ sub, selected, apiService, token }) => {

    const dispatch = useDispatch();
    const [ check, setCheck ] = useState(selected);

    const add = (id) => {
        apiService.getResources('add-interest?id='+id, token).then(res=>{
            setCheck(true);
        });
    };

    const remove = (id) => {
        apiService.getResources('remove-interest?id='+id, token).then(res=>{
            setCheck(false);
        });
    };

    return <SubBox selected={check} onPress={()=>check?remove(sub.id):add(sub.id)}>
        <SubText>{sub.title}</SubText><Icon name="close" width={16} height={16}/>
    </SubBox>
};

const Box = styled.ScrollView`
    background-color: #e8eaed;
    padding: 12px;
`;
const Title = styled.Text`
    flex: 1;
    color: ${Colors.tintColor}
    font-size: 16px;
    font-family: LatoBold;
`;
const CategoryBox = styled.View`
    background-color: #ffffff;
    padding: 15px;
    border-bottom-color: #ececec;
    border-bottom-width: 1px;
`;
const SubBox = styled.TouchableOpacity`
    padding: 2px 6px;
    border-width: 2px;
    border-radius: 32px;
    margin-bottom: 12px;
    flex-direction: row;
    align-items: center;
    border-color: ${({selected}) => selected ? Colors.gold : Colors.white};
`;
const Subs = styled.View`
    padding: 10px 0px;
`;
const SubText = styled.Text`
    flex: 1;
    padding: 5px;
    color: ${Colors.tintColor}
    font-size: 16px;
    font-family: Lato;
`;
const Header = styled.TouchableOpacity`
    flex-direction: row;
`;

export default withApiService()(withNavigationFocus(InterestsScreen));
