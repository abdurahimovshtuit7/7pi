import React  from 'react';
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";
import {withTranslation} from "react-i18next";

 function SelectCategory({ navigation,t}) {

  const next = navigation.getParam('next');
  const action = navigation.getParam('action');
  const params = navigation.getParam('params');

  return <Box>
    <Title>{t("select_category")}</Title>
    <ItemListContainer url={'market'} type={'categories'} next={next} action={action} params={params}/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
const Title = styled.Text`
    font-size: 24px;
    padding: 10px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;

export default withTranslation('main')(SelectCategory)
