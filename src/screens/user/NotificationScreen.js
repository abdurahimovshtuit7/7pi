import React, { useState } from 'react';
import styled from "styled-components/native/dist/styled-components.native.esm";
import Colors from "../../constants/Colors";
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";

const NotificationScreen = () => {

  const [ refresh, setRefresh ] = useState(undefined);

  return <Box>
    <ItemListContainer url={'notification'} type="notifications" refresh={refresh} setRefresh={setRefresh}/>
  </Box>;
};

export default NotificationScreen;

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
