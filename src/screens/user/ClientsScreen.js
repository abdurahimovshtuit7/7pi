import React, { useState, useEffect } from 'react';
import styled from "styled-components/native/dist/styled-components.native.esm";
import Colors from "../../constants/Colors";
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";
import { withNavigationFocus } from "react-navigation";

function ClientsScreen({ navigation, isFocused }) {

  const action = navigation.getParam('action');
  const params = navigation.getParam('params');

  const [ refresh, setRefresh ] = useState(new Date());

  return <Box>
    <ItemListContainer url={'clients'} type="clients" action={action?action:"delete"} params={params} refresh={refresh} setRefresh={setRefresh} isFocused={isFocused}/>
  </Box>;
}

ClientsScreen.navigationOptions = {
  header: null,
};

export default withNavigationFocus(ClientsScreen);

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
