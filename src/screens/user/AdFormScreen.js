import React, { useState, useEffect } from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Alert} from 'react-native'
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Input from "../../components/Input";
import Button from "../../components/button/Button";
import withApiService from "../../components/hoc";
import { withNavigation } from "react-navigation";
import {withTranslation} from "react-i18next";
import { Dropdown } from "react-native-material-dropdown-v2";
import CheckBox from "../../components/CheckBox";
import Spinner from "../../components/spinner/Spinner";
import mime from "mime";
import Toast from "../../components/Toast";
import {itemsFetch} from "../../actions";



function AdFormScreen({ apiService, navigation,t }) {

  const _apiBase = 'https://7pi.uz/rest/';
  const id = navigation.getParam('id');

  const ad = navigation.getParam('ad');

  const setRefresh = navigation.getParam('setRefresh');

  const token = useSelector(state => state.auth.token);
    const dispatch = useDispatch();

  const [ wm, setWm ] = useState(false);

  const [ cat, setCat ] = useState(null);
  const [progress,setProgress] =useState(false)

  const [ regions, setRegions ] = useState([]);

  const [form, setState] = useState({
    title: "",
    text: "",
    price: "",
    pricen: 0,
    pricer: false,
    city: 30,
    address: "",
    phone: "",
    email: "",
    shop: 0,
    photo: null,
  });

  const updateField = (e,param) => {
    setState({
      ...form,
      [param]: e
    });
  };
  // const {t} = props
  // console.log("TTTTTTT:",t('top'))

  useEffect(()=>{
    let regs = [];
    ad?
        apiService.getResources('edit-offer?id='+ad,token)
            .then(res=>{
              setWm(!res.canAdd);
              updateField(res.model.address,'address');
              res.regions.map(item=>{
                regs.push({value:item.id,label:item.name});
              });
              setRegions(regs);
              updateField(res.model.title,'title');
              updateField(res.model.text,'text');
              updateField(res.model.price,'price');
              updateField(res.model.phone,'phone');
              updateField(res.model.email,'email');
              setCat(res.model.catName+" > "+res.model.parentCatName);
            })
            .catch() :
        apiService.getResources('add-offer?id='+id,token)
            .then(res=>{
              setWm(!res.canAdd);
              updateField(res.model.address,'address');
              res.regions.map(item=>{
                regs.push({value:item.id,label:item.name});
              });
              setRegions(regs);
            })
  },[]);

  useEffect(()=>{
    console.log(form.title);
  },[form]);

  const save = () => {

      const url = id?'add-offer?id='+id:'add-offer?id='+form.cat;

      let bodyFormData = new FormData();
      setProgress(true)
      console.log("URL:",url)
      bodyFormData.append("Ad[title]", form.title);
      bodyFormData.append("Ad[text]", form.text);
      bodyFormData.append("Ad[price]", form.price);
      bodyFormData.append("Ad[pricen]", form.pricen);
      bodyFormData.append("Ad[pricer]", form.pricer);
      bodyFormData.append("Ad[city]", form.city);
      bodyFormData.append("Ad[address]", form.address);
      bodyFormData.append("Ad[phone]", form.phone);
      bodyFormData.append("Ad[email]", form.email);
      bodyFormData.append("Ad[shop]", form.shop);

      const photo = form.photo?form.photo.split('/'):null;
      if (form.photo) {
        bodyFormData.append(`Ad[photos][]`, {
          name: form.photo.name ? form.photo.name : photo[photo.length - 1],
          filename: form.photo.filename ? form.photo.filename : photo[photo.length - 1],
          type: mime.getType(form.photo),
          uri: form.photo
        });
        // bodyFormData.append("Ad[photos][]", form.photo);
      }
      console.log("Body:",bodyFormData)
      return fetch(_apiBase+url,{
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        },
        body: bodyFormData,
      }).then(res =>{
          setProgress(false)
          Alert.alert(t("success"),null,[],{cancelable: true});
          itemsFetch(apiService,dispatch,'shop',"shop",token);

          navigation.navigate("MyShop");
      }).catch(err=>console.log(err))
    // apiService.createAd(form,token,id).then(res=>{
    //   console.log("RESP:",res)
    //   setRefresh(new Date());
    //   navigation.goBack();
    // });
  };

  return form.title!==undefined?<Box>
    <Title>{cat?cat:t("announcement")}</Title>
    <Input value={form.title} label={t("name")} onEdit={val=>updateField(val,'title')}/>
    <Input value={form.text} label={t("tavsif")} multiline={true} numberOfLines={3} onEdit={val=>updateField(val,'text')}/>
    <Input value={form.price} label={t("price")} onEdit={val=>updateField(val,'price')}/>
    <Row>
        <Dropdown
            containerStyle={{flex:0.5,paddingRight:5}}
            value={form.pricen}
            label={t("currency")}
            data={[{value:0,label:t("sum")},{value:1,label:"у.е."}]}
            onChangeText={val=>updateField(val,'pricen')}/>
        <CheckBox text={t("договорная")} onPress={val=>updateField(val,'pricer')}/>

    </Row>
    <Dropdown
        value={form.city}
        data={regions}
        label={t("region")}
        onChangeText={val=>updateField(val,'city')}
    />
    <Input value={form.address} label={t("address")} onEdit={val=>updateField(val,'address')}/>
    <Input value={form.phone} label={t("phone")} type={"phone"} onEdit={val=>updateField(val,'phone')}/>
    <Input value={form.email} label={t("email")} onEdit={val=>updateField(val,'email')}/>
    <Input label={t("picture")} icon="photo" photo={true} onEdit={val=>updateField(val,'photo')}/>
    <Button text={t("addStore")} type="green" progress={progress} onPress={save}/>
    <Text>{t("desc")}</Text>
    <Button text={t("addMarket")} type="primary" progress={progress} onPress={save}/>
    <Text>{t("inTheStore")}</Text>
    <Title/>
    <Title/>
  </Box>:<Spinner/>;
}

AdFormScreen.navigationOptions = { header: null };
// AdFormScreen = withTranslation('main')(AdFormScreen)
export default withTranslation('main')(withApiService()(withNavigation(AdFormScreen)));

const Box = styled.ScrollView`
  flex: 1;
  padding: 16px;
  background: ${Colors.white};
`;
const Title = styled.Text`
    font-size: 24px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;
const Col = styled.Text`

    flex-direction: row;
`;
const Text = styled.Text`
    font-size: 14px;
    font-family: Lato;
    color: rgba(27,28,41,.75);
`;

const Row = styled.View`
     flex-direction: row;
     justify-content:space-between;
     align-items:center;
`
