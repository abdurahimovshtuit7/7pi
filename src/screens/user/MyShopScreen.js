import React, {useEffect} from 'react';
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";
import Button from "../../components/button/Button";
import {withTranslation} from "react-i18next";
import {withNavigation} from "react-navigation";
import {itemsFetch} from "../../actions";
import {useDispatch, useSelector} from "react-redux";
import withApiService from "../../components/hoc";

 function MyShopScreen({ apiService,navigation,t}) {
     const dispatch = useDispatch();
     const state = useSelector(state => state);
     // const lang = useSelector(state => state.lang);
     const token = state.auth.token;

     useEffect(()=>{

     },[])
  return <Box>
    <Button onPress={()=>navigation.navigate('SelectCategory',{next:'AdForm'})} type="green" text={t("add")} icon="plus"/>
    <ItemListContainer url={'shop'} type="shop" action="delete"/>
  </Box>;
}

MyShopScreen.navigationOptions = {
  header: null,
};

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;

export default withApiService()(withNavigation(withTranslation('main')(MyShopScreen)))
