import React, { useState, useEffect } from 'react';
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Button from "../../components/button/Button";
import Icon from "../../constants/Icons";
import { Keyboard, Platform, Image } from "react-native"
import ItemListContainer from "../../containers/item-list-container/ItemListContainer";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {useDispatch, useSelector} from "react-redux";
import withApiService from "../../components/hoc";
import * as ImageManipulator from 'expo-image-manipulator';
import {profileFetch} from "../../actions";
import Spinner from "../../components/spinner/Spinner";
import {withTranslation} from "react-i18next";

function FeedScreen({ apiService, navigation,t }) {

  const [ image, setImage ] = useState(null);
  const [ text, setText ] = useState('');
  const [ disabled, setDisabled ] = useState(false);
  const [ refresh, setRefresh ] = useState(undefined);
  const token = useSelector(state => state.auth.token);
  const feedUpdate = useSelector(state => state.feed.time);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    if(!user) profileFetch(apiService,dispatch,token);
    const listener = navigation.addListener('willFocus',() => {
      setRefresh(feedUpdate);
    });
    return () => listener.remove();
  },[feedUpdate]);

  const _pickImage = async () => {

    if (Platform.OS === 'ios') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      base64: true,
      quality: 1
    });

    if (!result.cancelled) {
      let width = 1280; let height = 1280; let aspect = 1;

      if(result.width > result.height) {
        aspect = result.width / 1280 ;
        height = result.height / aspect;
      }else{
        aspect = result.height / 1280 ;
        width = result.width / aspect;
      }

      ImageManipulator.manipulateAsync(result.uri, [{resize:{width,height}}], {compress:0.5, format:ImageManipulator.SaveFormat.JPEG,base64:true}).then(res=>{
        setImage(res);
      });
    }
  };

  const add = () => {
    setDisabled(true);
    apiService.addPost(text,token,image).then(res=>{
      setRefresh(new Date());
      setText(null);
      setImage(null);
      setDisabled(false);
      Keyboard.dismiss();
    });
  };

  if(!user) return <Spinner/>;

  return <Box contentContainerStyle={{flexGrow: 1}}
              keyboardShouldPersistTaps='handled'>
    <Form>
      <TextInput
          value={text}
          multiline={true}
          numberOfLines={3}
          onChangeText={val=>setText(val)}
          onBlur={Keyboard.dismiss}
          placeholder={t("note")}
      />
      <PhotoInput onPress={_pickImage}>
        {image?
            <Image source={{uri:image.uri}} style={{ width: 48, height: 48 }}/>
            :<Icon name="photo" width={48} height={48}/>}
      </PhotoInput>
    </Form>
    <Col>
      <Button text={t("offer")} onPress={add} type="green" icon="plus" flex={true} disabled={disabled}/>
      <Button text={t("onNote")} onPress={add} type="primary" icon="note" flex={true} disabled={disabled}/>
    </Col>
    <ItemListContainer url="api?start=0" type="feed" refresh={refresh} setRefresh={setRefresh}/>
  </Box>;
}

FeedScreen.navigationOptions = {
  header: null,
};

export default withTranslation('main')(withApiService()(FeedScreen));

const Form = styled.View`
    margin: 16px 16px 8px 16px;
    flex-direction: row;
    background-color: #fff;
    border-radius: 6px;
`;
const TextInput = styled.TextInput`
    padding: 8px 16px;
    font-size: 16px;
    flex: 1;
`;
const PhotoInput = styled.TouchableOpacity`
    padding: 16px 12px;
    border-left-color: #ddd;
    align-items: center;
    border-left-width: 1px;
    box-shadow: 0px 4px 50px rgba(0, 0, 0, 0.1);
`;
const Col = styled.View`
    flex-direction: row;
    margin: 0px 10px;
`;

const Box = styled.ScrollView`
  flex: 1;
  background: ${Colors.bg};
`;
