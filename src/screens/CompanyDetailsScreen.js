import React from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import CompanyContainer from "../containers/item-container/CompanyContainer";

export default function CompanyDetailsScreen({ navigation }) {

  const id = navigation.getParam('id');

  return <Box>
    <CompanyContainer id={id}/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
