import React, {Component} from 'react';
import {connect} from 'react-redux';
// import AnimatedEllipsis from 'react-native-animated-ellipsis';
// import NavigationService from 'navigators/NavigationService';
import Launch from './index_without_connect';
import i18next from 'i18next';
// import {CommonActions} from '@react-navigation/native';
import { StackActions, NavigationActions } from 'react-navigation';

import {Platform, StatusBar} from 'react-native';
// import colors from 'assets/styles/colors';

class LaunchScreen extends Component {
    getData() {
        setTimeout(() => {
            i18next.changeLanguage(this.props.currentLangCode);

            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Root' })],
            });
            this.props.navigation.dispatch(resetAction);
        }, 1000);
    }

    componentDidMount() {
        this.getData();
    };

    render() {
        // console.log("Render:",this.props.navigation)
        return (
            <>
                <Launch/>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.lang,
    };
};
export default connect(mapStateToProps)(LaunchScreen);
