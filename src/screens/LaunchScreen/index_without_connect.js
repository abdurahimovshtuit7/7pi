import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
    View,
    Text,
    ActivityIndicator,
    Platform, StatusBar
} from 'react-native'
import styles from './styles'
// import colors from "../../assets/styles/colors";
import {I18nextProvider} from "react-i18next";

class LaunchScreen extends Component {

    render () {
        // console.warn(JSON.stringify(this.props.login))
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={"#000"}
                    barStyle="light-content"
                    translucent={false}
                />
                {/*<Text style={styles.brandName}>*/}
                {/*    7Pi*/}
                {/*</Text>*/}
                <View style={styles.activeIndicator}>
                    <ActivityIndicator  color={"#fff"}/>

                </View>
                {/*<Text style={styles.loading}>*/}
                {/*    Loading*/}

                {/*</Text>*/}
                <Text style={styles.footer}>

                </Text>
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {

    }
}
export default connect(mapStateToProps)(LaunchScreen)
