import React from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import ItemContainer from "../containers/item-container/ItemContainer";

export default function ProductDetailsScreen({ navigation }) {

  const id = navigation.getParam('id');

  return <Box>
    <ItemContainer id={id}/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
