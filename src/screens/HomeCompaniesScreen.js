import React  from 'react';
import styled from "styled-components/native";
import Colors from "../constants/Colors";
import ItemListContainer from "../containers/item-list-container/ItemListContainer";

export default function HomeCompaniesScreen() {

  return <Box>
    <ItemListContainer url={'market'} type={'categories'} next={'companies'} />
  </Box>;
}

HomeCompaniesScreen.navigationOptions = {
  header: null,
};

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
