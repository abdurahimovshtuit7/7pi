import React from 'react';
import styled from "styled-components/native/dist/styled-components.native.esm";
import Colors from "../constants/Colors";
import ItemListContainer from "../containers/item-list-container/ItemListContainer";

export default function CompaniesScreen({ navigation }) {

  const id = navigation.getParam('id');
  const action = navigation.getParam('action');
  const params = navigation.getParam('params');

  return <Box>
    <ItemListContainer url={'companies?id='+id} type="companies" action={action} params={params}/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
