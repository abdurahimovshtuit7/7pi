import React from 'react';
import ItemListContainer from "../containers/item-list-container/ItemListContainer";
import styled from "styled-components/native";
import Colors from "../constants/Colors";

export default function CategoriesScreen({ navigation }) {

  const id = navigation.getParam('id');
  const next = navigation.getParam('next');
  const action = navigation.getParam('action');
  const params = navigation.getParam('params');

  return <Box>
    <ItemListContainer url={'market/'+id} type={'subcategories'} next={next} action={action} params={params}/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
