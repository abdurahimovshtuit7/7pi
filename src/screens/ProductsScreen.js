import React from 'react';
import ItemListContainer from "../containers/item-list-container/ItemListContainer";
import styled from "styled-components/native";
import Colors from "../constants/Colors";

export default function ProductsScreen({ navigation }) {

  const id = navigation.getParam('id');

  return <Box>
    <ItemListContainer url={'ads/'+id} type="products"/>
  </Box>;
}

const Box = styled.View`
  flex: 1;
  background: ${Colors.bg};
`;
