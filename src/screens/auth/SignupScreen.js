import React from 'react';
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Input from "../../components/Input";
import Button from "../../components/button/Button";
import withApiService from "../../components/hoc";
import {connect} from "react-redux";
import {Linking} from "expo";
import Icon from "../../constants/Icons";
import R from "../../constants/R";
import {signup} from "../../actions";
import {withTranslation} from "react-i18next";

class SignupScreen extends React.Component{

  state = {
    name: '',
    surname: '',
    email: '',
    password: '',
    password_repeat: '',
    phone: ''
  };

  render() {

    const { token, navigation,t,signUP } = this.props;

    if(token) {
      navigation.popToTop();
    }
   console.log("signUP:",this.props)
    return <Box>
      <Title>
        {/*<Icon name="user" width={48} height={48} fill={Colors.blue}/>*/}
        {t("sign_up")}</Title>
      <Input label={t("email")} autoFocus={true} value={this.state.email} onEdit={e=>this.setState({email:e})}/>
      <Input label={t("phone")} type={"phone"} value={this.state.phone} onEdit={e=>this.setState({phone:e})}/>
      <Input label={t("first_name")} value={this.state.name} onEdit={e=>this.setState({name:e})}/>
      <Input label={t("last_name")} value={this.state.surname} onEdit={e=>this.setState({surname:e})}/>
      <Input label={t("password")} secure={true} value={this.state.password} onEdit={e=>this.setState({password:e})}/>
      <Input label={t("confirm_password")} secure={true} value={this.state.password_repeat} onEdit={e=>this.setState({password_repeat:e})}/>
      <Link onPress={()=>Linking.openURL('https://7pi.uz/about/terms')}><LinkText>{t("agreement")}</LinkText>
        <Icon name="caret" fill={R.colors.blue} width={24} height={24} style={{alignSelf:"center"}} /></Link>
      <Button text={t("register")} type="primary" onPress={()=>signUP(this.state)}/>
      <Text>{t("register_description")}</Text>
      <Button onPress={()=>Linking.openURL('https://7pi.uz/about/plans')} text="Посмотреть тарифы" type="primary"/>
      <Br/>
    </Box>;
  }
}

const mapStateToProps = ({ auth: { token, error, loading } }) => {
  return { token, error, loading }
};

const mapDispatchToProps = (dispatch, { apiService }) => {

  return{
    signUP: (form) => signup( apiService, dispatch, form )
  }
};

export default withApiService()(withTranslation('main')(connect(mapStateToProps, mapDispatchToProps)(SignupScreen)));

const Box = styled.ScrollView`
  flex: 1;
  padding: 16px;
  background: ${Colors.white};
`;
const Title = styled.Text`
    font-size: 24px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;
const Text = styled.Text`
    font-size: 14px;
    font-family: Lato;
    margin-bottom: 10px;
    color: ${Colors.text};
`;
const LinkText = styled.Text`
    font-size: 14px;
    flex: 1;
    font-family: Lato;
    margin-bottom: 10px;
    color: ${Colors.ocean};
`;
const Link = styled.TouchableOpacity`
    color: ${Colors.ocean};
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;
const Br = styled.View`
    height: 44px;
    width: 100%;
`;
