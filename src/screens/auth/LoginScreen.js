import React from 'react';
import {connect, useDispatch, useSelector} from "react-redux";
import styled from "styled-components/native";
import Colors from "../../constants/Colors";
import Input from "../../components/Input";
import Button from "../../components/button/Button";
import withApiService from "../../components/hoc";
import { login } from "../../actions";
import Spinner from "../../components/spinner/Spinner";
import {Linking} from "expo";
import {withTranslation} from "react-i18next";

class LoginScreen extends React.Component{

  state = {
    username: '',
    password: ''
  };

  render() {

    const { navigation, login, loading, token,t } = this.props;

    if(token) navigation.navigate('Home');

    if(loading) return <Spinner/>;

    return <Box>
      <Title>{t("kirish")}</Title>
      <Input label={t("email")} autoFocus={true} autoCompleteType="email" value={this.state.username} onEdit={username=>this.setState({username})}/>
      <Input label={t("password")} secure={true} value={this.state.password} onEdit={password=>this.setState({password})}/>
      <Button text={t("goto_cabinet")} type="primary" onPress={()=>login(this.state)}/>
      <Button text={t("register")} type="green" icon="plus" onPress={()=>navigation.navigate('Signup')}/>
      <Button text={t("restore_password")} onPress={()=>Linking.openURL('https://7pi.uz/recover')} type="link"/>
    </Box>;
  }
}

const mapStateToProps = ({ auth: { token, error, loading } }) => {
  return { token, error, loading }
};

const mapDispatchToProps = (dispatch, { apiService }) => {

  return{
    login: (form) => login( apiService, dispatch, form )
  }
};

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withTranslation('main')(LoginScreen)));

const Box = styled.ScrollView`
  flex: 1;
  padding: 16px;
  background: ${Colors.white};
`;
const Title = styled.Text`
    font-size: 24px;
    font-family: LatoBold;
    color: ${Colors.blue};
`;
